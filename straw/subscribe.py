""" subscribe.py

Module for handling feed subscription process.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """ GNU General Public License

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """


import os, os.path
import urlparse
import urllib
import pygtk
pygtk.require('2.0')
import gtk
from gtk.glade import XML
import gettext
import feeds
import error
import Event
import SummaryParser
import ParsedSummary
import time
import URLFetch
import MVP
import Config

import straw
from straw import helpers

class SubscribeView(MVP.GladeView):
    def _initialize(self):
        self._window         = self._widget.get_widget('subscribe_dialog')
        self._location_entry = self._widget.get_widget('feed_location_entry')
        self._username_entry = self._widget.get_widget('username_entry')
        self._password_entry = self._widget.get_widget('password_entry')
        self._error_text     = self._widget.get_widget('error_text')
        self._error_box      = self._widget.get_widget('error_box')
        self._location_entry.grab_focus()

    def _on_ok_clicked(self, *args):
        location = self._location_entry.get_text()
        if not location:
            self.display_error(_("Feed Location must not be empty"))
            return
        username = self._username_entry.get_text()
        username = username.strip()
        password = self._password_entry.get_text()
        password = password.strip()

        config = Config.get_instance()
        if config.offline:
            self._window.hide()
            response = helpers.report_offline_status(self._window)
            if response == gtk.RESPONSE_CANCEL:
                self.show()
                return
            config.offline = not config.offline
        self._presenter.subscribe(location, username, password)
        self._window.hide()

    def _clear_entries(self):
        self._location_entry.delete_text(0,-1)
        self._username_entry.delete_text(0,-1)
        self._password_entry.delete_text(0,-1)

    def show(self):
        self._clear_entries()
        self._error_box.set_property('visible',False)
        self._window.show()

    def display_error(self, text):
        self._error_box.set_property('visible',True)
        self._error_text.set_text(text)

    def set_location_entry(self, location):
        self._location_entry.set_text(location)

    def set_parent(self, parent):
        self._window.set_transient_for(parent)

    def _on_close_clicked(self, *args):
        self._window.hide()

    def _on_feed_location_entry_key_press_event(self, widget, event):
        if event.keyval == gtk.keysyms.Return:
            self._presenter.subscribe(self._location_entry.get_text())

class SubscribePresenter(MVP.BasicPresenter):

    def _normalize(self, url):
        u = urlparse.urlsplit(url.strip())
        # we check if 'scheme' is not empty here because URIs like
        # "google.com" IS valid but, in this case, 'scheme' is empty because
        # urlsplit() expects urls are in the format of scheme://netloc/...
        if not u[0] or (u[0] != "http" and u[0] != "feed"):
            return None
        if u[0] == 'feed':
            u = urlparse.urlsplit(u[2])
        # .. if that happens then we munge the url by adding a // and assume
        # that 'http' is the scheme.
        if u[1] == '':
             u = urlparse.urlsplit("//" + url, 'http')
        return u

    def subscribe(self, location, username=None, password=None):
        self._username = username
        self._password = password
        self._location = location

        consumer = SubscribeConsumer(self)

        try:
            URLFetch.get_instance().request(location, consumer,
                                            user=username, password=password)
        except Exception, e:
            consumer.http_failed(e)

    def set_parent(self, parent):
        self.view.set_parent(parent)

    def get_credential(self):
        return (self._location, self._username, self._password)

    def show(self, url=None):
        if url:
            self.view.set_location_entry(url)
        self.view.show()

#    def set_url_dnd(self, url):
#        self._window.show()
#        self._feed_location_presenter.view.find_feed(url)

    def set_location_from_clipboard(self):
        def _clipboard_cb(cboard, url, data=None):
            if url:
                url = self._normalize(url)
                if url and url[0] == "http":
                    url = urlparse.urlunsplit(url)
                    self.view.set_location_entry(url)
        clipboard = gtk.clipboard_get(selection="CLIPBOARD")
        clipboard.request_text(_clipboard_cb, None)

class SubscribeConsumer:

    def __init__(self, presenter):
        self._presenter = presenter

    def http_results(self, status,  data):
        self.process(data)

    def process(self, data):
        url, user, password = self._presenter.get_credential()
        feed = feeds.Feed.create_new_feed("", url, user, password)
        parsed = SummaryParser.parse(data, feed)
        if parsed.title == "":
           # store url loc in title in case it's empty
            parsed.title = urlparse.urlsplit(url.strip())[1]
        feed.title = helpers.convert_entities(parsed.title)
        feed.channel_description = helpers.convert_entities(parsed.description)
        feed.access_info = (url, user, password)
        feed.last_poll = int(time.time())
        fclist = feeds.category_list
        print feed
        fclist.append_feed(feed)
        feed.router.route_all(parsed)
        feed.poll_done()

    def http_failed(self, exception):
        # XXX signal to the user that subscription failed? incl. erros
        print "subscribe failed", exception

    def operation_stopped(self):
        # XXX signal that it stopped, why it stopped, and stop all progress
        # bars, etc...
        print "subscribe operation stopped"


_dialog = None
def subscribe_show(url=None, parent=None):
    global _dialog
    if not _dialog:
        gladefile = XML(os.path.join(straw.STRAW_DATA_DIR, "subscribe.glade"))
        _dialog = SubscribePresenter(view=SubscribeView(gladefile))
    _dialog.set_parent(parent)
    _dialog.set_location_from_clipboard()
    _dialog.show()
