""" FeedListView.py

Module for displaying the feeds in the Feeds TreeView.
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import os, copy, locale, logging
import gobject
import gtk
import pango

import feeds
import Config
import PollManager
import MVP

import straw
from straw import helpers

class Column:
    pixbuf, name, foreground, unread, object = range(5)


class TreeNodeAdapter (gobject.GObject):
    ''' A Node Adapter which encapsulates either a Category or a Feed '''

    __gsignals__ = {
        'feed-changed' :     (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                              (gobject.TYPE_PYOBJECT,)),
        'category-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                              (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT,)),
        'category-feed-added' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                                 (gobject.TYPE_PYOBJECT,)),
        'category-feed-removed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                                  (gobject.TYPE_PYOBJECT,))
        }

    def __init__(self, object):
        gobject.GObject.__init__(self)
        self.obj = object
        filename = os.path.join(straw.STRAW_DATA_DIR, 'feed.png')
        self.default_pixbuf = gtk.gdk.pixbuf_new_from_file(filename)
        if isinstance(self.obj, feeds.Feed):
            self.obj.connect('changed', self.feed_changed_cb)
        elif isinstance(self.obj, feeds.FeedCategory):
            print repr(self.obj)
            self.obj.connect('feed-added', self.feed_added_cb)
            self.obj.connect('feed-removed', self.feed_removed_cb)
            self.obj.connect('changed', self.category_changed_cb)

    def feed_changed_cb(self, feed):
        self.emit('feed-changed', feed)

    def category_changed_cb(self, category, *args):
        logging.debug("TREENODEADAPTER CATEGORY_CHANGED_CB -> ", category, feed)
        self.emit('category-changed', category, args)

    def feed_added_cb(self, category, feed):
        logging.debug("TREE NODE ADAPTER FEED ADDED CB!!!", feed)
        self.emit('category-feed-added', feed)

    def feed_removed_cb(self, category, feed):
        self.emit('category-feed-removed', feed)

    def has_children(self):
        ''' Checks if the node has children. Essentially this means the object
        is a category.'''
        has_child = False
        try:
            has_child = self.obj.feeds and True or False
        except AttributeError:
            has_child = False
        return has_child

    @property
    def title(self):
        ''' The title of the node be it a category or a feed '''
        return self.obj.title

    @property
    def num_unread_items(self):
        ''' The number of unread items of the feed or if it's a category,
        the aggregate number of unread items of the feeds belonging to the
        category.'''
        def _r(a,b):return a + b
        try:
            unread_items = self.obj.number_of_unread
        except AttributeError:
            unread_items = reduce(_r, [feed.number_of_unread for feed in self.obj.feeds])
        return unread_items or ''

    @property
    def pixbuf(self):
        ''' gets the pixbuf to display according to the status of the feed '''
        widget = gtk.Label()
        # ignore why above is a gtk.Label. We just need
        # gtk.Widget.render_icon to get a pixbuf out of a stock image.
        # Fyi, gtk.Widget is abstract that is why we need a subclass to do
        # this for us.
        try:
            if self.obj.process_status is not feeds.Feed.STATUS_IDLE:
                return widget.render_icon(gtk.STOCK_EXECUTE, gtk.ICON_SIZE_MENU)
            elif self.obj.error:
                return widget.render_icon(gtk.STOCK_DIALOG_ERROR, gtk.ICON_SIZE_MENU)
        except AttributeError, ex:
            logging.exception(ex)
            return widget.render_icon(gtk.STOCK_DIRECTORY, gtk.ICON_SIZE_MENU)
        return self.default_pixbuf

    @property
    def feed(self):
        ''' An alias to a Feed object '''
        if not isinstance(self.obj, feeds.Feed):
            raise TypeError, _("object is not of a Feed")
        return self.obj

    @property
    def category(self):
        ''' An alias to a Category object '''
        if not isinstance(self.obj, feeds.FeedCategory):
            raise TypeError, _("object is not a Category")
        return self.obj

class FeedListModel:
    ''' The model for the feed list view '''

    def __init__(self):
        # name, pixbuf, unread, foreground
        self.store = gtk.TreeStore(gtk.gdk.Pixbuf, str, str, str, gobject.TYPE_PYOBJECT)
        # unread, weight, status_flag feed object, allow_children
        # str, int, 'gboolean', gobject.TYPE_PYOBJECT, 'gboolean'
        self.fclist = feeds.category_list

        # build the user categories and its feeds
        populate = self.populate
        for category in self.fclist.user_categories:
            populate(category, category.feeds)
        populate(self.fclist.un_category, self.fclist.un_category.feeds)

    def populate(self, category, feeds):
        parent = None
        if category:
            parent = self.store.append(parent)
            node_adapter = TreeNodeAdapter(category)
            node_adapter.connect('category-changed', self.category_changed_cb)
            node_adapter.connect('category-feed-added', self.feed_added_cb)
            node_adapter.connect('category-feed-removed', self.feed_removed_cb)
            self.store.set(parent, Column.pixbuf, node_adapter.pixbuf,
                           Column.name, node_adapter.title,
                           Column.unread, node_adapter.num_unread_items,
                           Column.object, node_adapter)
        for f in feeds:
            rowiter = self.store.append(parent)
            print f
            node_adapter = TreeNodeAdapter(f)
            node_adapter.connect('feed-changed', self.feed_changed_cb)
            self.store.set(rowiter, Column.pixbuf, node_adapter.pixbuf,
                           Column.name, node_adapter.title,
                           Column.foreground, node_adapter.num_unread_items and 'blue' or 'black',
                           Column.unread, node_adapter.num_unread_items,
                           Column.object, node_adapter)

    def __getattribute__(self, name):
        attr = None
        try:
            attr = getattr(self, name)
        except AttributeError, ae:
            attr = getattr(self.store, name)
        else:
            return attr

    def category_changed_cb(self, node_adapter, *args):
        # XXX What changes do we need here? new feed? udated subscription?
        print "FEED LIST MODEL -> ", node_adapter, args

    def feed_added_cb(self, node_adapter, feed):
        logging.debug("FEED ADDED !", node_adapter, feed)
        if node_adapter.category is self.fclist.all_category: #XXX should really fix this
            return
        treemodelrow = self.search(self.store,
                                   lambda r, d: r[d[0]] == d[1],
                                   (Column.object, node_adapter))
        feed_node_adapter = TreeNodeAdapter(feed)
        feed_node_adapter.connect('feed-changed', self.feed_changed_cb)
        self.store.append(treemodelrow.iter, [feed_node_adapter.pixbuf,
                                              feed_node_adapter.title,
                                              feed_node_adapter.num_unread_items and 'blue' or 'black',
                                              feed_node_adapter.num_unread_items,
                                              feed_node_adapter])

    def feed_removed_cb(self, node_adapter, feed):
        logging.debug("FEED REMOVED CB ", node_adapter, feed)

    def feed_changed_cb(self, node_adapter, feed):
        row = self.search(self.store,
                          lambda r, data: r[data[0]] == data[1],
                          (Column.object, node_adapter))
        if not row: return
        path = self.store.get_path(row.iter)
        self.store[path] = [node_adapter.pixbuf, node_adapter.title,
                            node_adapter.num_unread_items and 'blue' or 'black',
                            node_adapter.num_unread_items,
                            node_adapter]

    @property
    def model(self):
        return self.store

    def search(self, rows, func, data):
        if not rows: return None
        for row in rows:
            if func(row, data):
                return row
            result = self.search(row.iterchildren(), func, data)
            if result: return result
        return None

class FeedsView(MVP.WidgetView):
    def _initialize(self):
        self._widget.set_search_column(Column.name)

        # pixbuf column
        column = gtk.TreeViewColumn()
        unread_renderer = gtk.CellRendererText()
        column.pack_start(unread_renderer, False)
        column.set_attributes(unread_renderer,
                              text=Column.unread)

        status_renderer = gtk.CellRendererPixbuf()
        column.pack_start(status_renderer, False)
        column.set_attributes(status_renderer,
                              pixbuf=Column.pixbuf)

        # feed title renderer
        title_renderer = gtk.CellRendererText()
        column.pack_start(title_renderer, False)
        column.set_attributes(title_renderer,
                              foreground=Column.foreground,
                              text=Column.name) #, weight=Column.BOLD)

        self._widget.append_column(column)

        selection = self._widget.get_selection()
        selection.set_mode(gtk.SELECTION_MULTIPLE)

        self._widget.connect("button_press_event", self._on_button_press_event)
        self._widget.connect("popup-menu", self._on_popup_menu)

        uifactory = helpers.UIFactory('FeedListActions')
        action = uifactory.get_action('/feedlist_popup/refresh')
        action.connect('activate', self.on_menu_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/mark_as_read')
        action.connect('activate', self.on_menu_mark_all_as_read_activate)
        action = uifactory.get_action('/feedlist_popup/stop_refresh')
        action.connect('activate', self.on_menu_stop_poll_selected_activate)
        action = uifactory.get_action('/feedlist_popup/remove')
        action.connect('activate', self.on_remove_selected_feed)
        action = uifactory.get_action('/feedlist_popup/properties')
        action.connect('activate', self.on_display_properties_feed)
        self.popup = uifactory.get_popup('/feedlist_popup')


    def _model_set(self):
        self._widget.set_model(self._model.model)

    def add_selection_changed_listener(self, listener):
        selection = self._widget.get_selection()
        selection.connect('changed', listener.feedlist_selection_changed, Column.object)

    def _on_popup_menu(self, treeview, *args):
        self.popup.popup(None, None, None, 0, 0)

    def _on_button_press_event(self, treeview, event):
        retval = 0
        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = gtk.get_current_event_time()
            path = treeview.get_path_at_pos(x, y)
            if path is None:
                return 1
            path, col, cellx, celly = path
            treeview.grab_focus()
            self.popup.popup(None, None, None, event.button, time)
            retval = 1
        return retval

    def foreach_selected(self, func):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            for treeiter in iters:
                object = model.get_value(treeiter, Column.object)
                func(object, model, treeiter)
        except TypeError, te:
            ## XXX maybe object is a category
            logging.exception(te)
        return

    def on_menu_poll_selected_activate(self, *args):
        config = Config.get_instance()
        poll = True
        if config.offline: #XXX
            config.offline = not config.offline
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        nodes = [model.get_value(treeiter,Column.object) for treeiter in iters]
        fds = []
        for n in nodes:
            try:
                fds.append(n.feed)
            except TypeError:
                fds += n.category.feeds
        if fds:
            PollManager.get_instance().poll(fds)
        return

    def on_menu_stop_poll_selected_activate(self, *args):
        self.foreach_selected(lambda o,*args: o.feed.router.stop_polling())

    def on_menu_mark_all_as_read_activate(self, *args):
        self.foreach_selected(lambda o,*args: o.feed.mark_items_as_read())

    def on_remove_selected_feed(self, *args):
        def remove(*args):
            (object, model, treeiter) = args
            model.remove(treeiter)
            feedlist = feeds.get_feedlist_instance()
            idx = feedlist.index(object.feed)
            del feedlist[idx]
        self.foreach_selected(remove)
        return

    def on_display_properties_feed(self, *args):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        path = pathlist.pop()
        node_adapter = self.model.model[path][Column.object]
        self._presenter.show_feed_information(node_adapter.feed)
        return

    def select_first_feed(self):
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        treeiter = model.get_iter_first()
        if not treeiter or not model.iter_is_valid(treeiter):
            return False
        self.set_cursor(treeiter)
        return True

    def select_next_feed(self, with_unread=False):
        ''' Scrolls to the next feed in the feed list

        If there is no selection, selects the first feed. If multiple feeds
        are selected, selects the feed after the last selected feed.

        If unread is True, selects the next unread with unread items.

        If the selection next-to-be is a category, go to the iter its first
        child. If current selection is a child, then go to (parent + 1),
        provided that (parent + 1) is not a category.
        '''
        has_unread = False
        def next(model, current):
            treeiter = model.iter_next(current)
            if not treeiter: return False
            if model.iter_depth(current): next(model, model.iter_parent(current))
            path = model.get_path(treeiter)
            if with_unread and model[path][Column.unread] < 1:
                next(model, current)
            self.set_cursor(treeiter)
            return True
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current = iters.pop()
            if model.iter_has_child(current):
                iterchild = model.iter_children(current)
                # make the row visible
                path = model.get_path(iterchild)
                for i in range(len(path)):
                    self._widget.expand_row(path[:i+1], False)
                # select his first born child
                if with_unread and model[path][Column.unread] > 0:
                    self.set_cursor(iterchild)
                    has_unread = True
                else:
                    has_unread = next(model, current)
            has_unread = next(model,current)
        except IndexError:
            self.set_cursor(model.get_iter_first())
            has_unread = True
        return has_unread

    def select_previous_feed(self):
        ''' Scrolls to the previous feed in the feed list.

        If there is no selection, selects the first feed. If there's multiple
        selection, selects the feed before the first selected feed.

        If the previous selection is a category, select the last node in that
        category. If the current selection is a child, then go to (parent -
        1). If parent is the first feed, wrap and select the last feed or
        category in the list.
        '''
        def previous(model, current):
            path = model.get_path(current)
            treerow = model[path[-1]-1]
            self.set_cursor(treerow.iter)
        selection = self._widget.get_selection()
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        try:
            current_first = iters.pop(0)
            if model.iter_has_child(current_first):
                children = model.iter_n_children(current_first)
                treeiter = model.iter_nth_child(children - 1)
                self.set_cursor(treeiter)
                return
            previous(model, current_first)
        except IndexError:
            self.set_cursor(model.get_iter_first())
        return

    def set_cursor(self, treeiter, col_id=None, edit=False):
        if not treeiter:
            return
        column = None
        path = self._model.model.get_path(treeiter)
        if col_id:
            column = self._widget.get_column(col_id)
        self._widget.set_cursor(path, column, edit)
        self._widget.scroll_to_cell(path, column)
        self._widget.grab_focus()
        return

class FeedsPresenter(MVP.BasicPresenter):
    def _initialize(self):
        self.model = FeedListModel()
    #    self._init_signals()
        return

    def _init_signals(self):
        flist = feeds.get_feedlist_instance()
        #flist.signal_connect(Event.ItemReadSignal,
        #                     self._feed_item_read)
        #flist.signal_connect(Event.AllItemsReadSignal,
        #                     self._feed_all_items_read)
        #flist.signal_connect(Event.FeedsChangedSignal,
        #                     self._feeds_changed)
        #flist.signal_connect(Event.FeedDetailChangedSignal,
        #                     self._feed_detail_changed)
        #fclist = FeedCategoryList.get_instance()
        #fclist.signal_connect(Event.FeedCategorySortedSignal,
        #                      self._feeds_sorted_cb)
        #fclist.signal_connect(Event.FeedCategoryChangedSignal,
        #                      self._fcategory_changed_cb)

    def select_first_feed(self):
        return self.view.select_first_feed()

    def select_next_feed(self, with_unread=False):
        return self.view.select_next_feed(with_unread)

    def select_previous_feed(self):
        return self.view.select_previous_feed()

    def _sort_func(self, model, a, b):
        """
        Sorts the feeds lexically.

        From the gtk.TreeSortable.set_sort_func doc:

        The comparison callback should return -1 if the iter1 row should come before
        the iter2 row, 0 if the rows are equal, or 1 if the iter1 row should come
        after the iter2 row.
        """
        retval = 0
        fa = model.get_value(a, Column.OBJECT)
        fb = model.get_value(b, Column.OBJECT)

        if fa and fb:
            retval = locale.strcoll(fa.title, fb.title)
        elif fa is not None: retval = -1
        elif fb is not None: retval = 1
        return retval

    #def sort_category(self, reverse=False):
    #    self._curr_category.sort()
    #    if reverse:
    #        self._curr_category.reverse()
    #    return

    def show_feed_information(self, feed):
        straw.feed_properties_show(feed)
        return
