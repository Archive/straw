""" URLFetch_threaded.py

Module for retrieving data from a URL (a threaded version using urllib2).

"""
__copyright__ = "Copyright (c) 2007 Straw developers"
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import threading
import urllib2

import httplib2

from sys import exc_info
import os, os.path

from straw import MainloopManager
from MainloopManager import schedule
from straw import NetworkConstants
from straw import Config
from straw import defs

class CancelledException(Exception):
    """Operation is cancelled."""
    pass

class ConnectionThread(threading.Thread):
    """A thread that fetches a URL XXX fetch several URLs"""
    def __init__(self, uri, consumer, headers, client_object, group=None, target=None, name=None, *args, **kwargs):
        threading.Thread.__init__(self, group, target, name, args, kwargs)
        self.setDaemon(True)

        self._uri = uri
        self._consumer = consumer
        self._headers = headers
        self._httpclient = client_object

        self._cancelled = threading.Event()

    def run(self):
        """The main loop of the thread"""
        #print "Fetching %s..." % self._uri
        try:
            self._handle_request()
        finally:
            schedule(get_instance().request_done, self)
            #print "Finished with %s" % self._uri

    def cooperate(self):
        """
        This should be called periodically in the thread execution.
        The method checks whether cancellation has been requested
        and if so, raises CancelledException.
        """
        if self._cancelled.isSet():
            raise CancelledException

    def _handle_request(self):
        data = None
        try:
            self.cooperate()
            try:
                (response, data) = self._httpclient.request(self._uri, headers=self._headers)
            except httplib2.HttpLib2Error, exception:
                if hasattr(exception, 'response'):
                    response = exception.response
                elif hasattr(exception, 'content'):
                    data = exception.content
                else:
                    raise
        except CancelledException:
            schedule(self._consumer.operation_stopped)
        except:
            try:
                self.cooperate()  # last chance to notice cancellation
            except CancelledException:
                schedule(self._consumer.operation_stopped)
            else:
                schedule(self._consumer.http_failed, exc_info()[1])
        else:
            if not hasattr(response, 'status'):  # fake for non-http
                f.status = 200
                f.reason = "OK"
            schedule(self._consumer.http_results, (None, response.status, response.reason), data)

    def cancel(self):
        """
        This can be called to cancel the request.
        inter-thread safe but not instant
        XXX network operations can take a long time to timeout
        XXX call operation_stopped instantly?
        """
        self._cancelled.set()

class ConnectionManager:
    """A manager for threads that fetch URLs"""

    CACHE_DIR = os.path.join(Config.straw_home(), 'cache')

    def __init__(self):
        self._starting = []  # requests waiting to be started
        self._active = []    # requests running right now

    def request(self, uri, consumer, headers={}, user=None, password=None, priority=NetworkConstants.PRIORITY_DEFAULT):

        httpclient = httplib2.Http(ConnectionManager.CACHE_DIR)

        if user and password:
            httpclient.add_credentials(user,password)

        config = Config.get_instance()
        proxy = config.proxy
        if proxy.active:
            httpclient.set_proxy(proxy.host, proxy.port)

        if not headers:
            headers = {}
        headers['user-agent'] = 'Straw/%s' % defs.VERSION

        thread = ConnectionThread(uri, consumer, headers, httpclient)

        if len(self._active) < NetworkConstants.MAX_CONNECTIONS:
            self._active.append(thread)
            thread.start()
        else:
            self._starting.append(thread)

        return thread.cancel # inter-thread safe

    def request_done(self, request):
        """Called by the request when it is finished."""
        self._active.remove(request)
        if self._starting and len(self._active) < NetworkConstants.MAX_CONNECTIONS:
            thread = self._starting.pop(0)
            self._active.append(thread)
            thread.start()

def create_instance():
    return ConnectionManager()

connection_manager_instance = None
def get_instance():
    global connection_manager_instance
    if connection_manager_instance is None:
        connection_manager_instance = create_instance()
    return connection_manager_instance

if __name__ == '__main__':
    import sys, gobject
    gobject.threads_init()

    class Stub:
        def http_results(self, status, data):
            print status
            print info
            print "%s bytes of content" % len(data)
        def http_permanent_redirect(self, location):
            print "Redirected to %s" % location
        def http_failed(self, e):
            print str(e)
        def operation_stopped(self):
            print "Operation stopped"

    for uri in sys.argv[1:]:
        get_instance().request(uri, Stub())

    try:
        gobject.MainLoop().run()
    finally:
        print get_instance()._queue
