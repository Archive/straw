""" Application.py

This is the main module that binds everything together.

"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """ GNU General Public License

This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import os, gettext, getopt, sys
import time
from xml.sax import saxutils
import pygtk
pygtk.require('2.0')
import gobject, gtk, gtk.glade
import gnome, gconf

import Config
import error
from ItemView import ItemView
from ItemList import ItemListPresenter, ItemListView
from FeedListView import FeedsView, FeedsPresenter
from OfflineToggle import OfflineToggle
from MainloopManager import MainloopManager
#from Find import FindPresenter, FindView
import MVP
import ItemStore
import ImageCache
import PollManager


import straw
from straw import helpers
from straw import feeds

class StatusPresenter(MVP.BasicPresenter):
    def _initialize(self):
        self._mmgr = straw.get_status_manager()
        self._mmgr.connect('changed', self._display)

    def _display(self, *args):
        cid =  self._view.get_context_id("straw_main")
        self._view.pop(cid)
        self._view.push(cid, self._mmgr.read_message())
        return

class ErrorPresenter:
    def __init__(self, widget):
        self._widget = widget
        self._tooltips = gtk.Tooltips()
        self._text = ''
        #self._curr_feed = None
        #self._curr_category = None
        #fclist = FeedCategoryList.get_instance()
        #fclist.connect('category-changed', self._category_changed)

    def feedlist_selection_changed(self, selection, column):
        errortexts = None
        (model, pathlist) = selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        if not iters: return
        errorfeeds = []
        nodes = [model.get_value(treeiter, column) for treeiter in iters]
        try:
            errorfeeds = [node.feed for node in nodes if node.feed and node.feed.error]
            errortexts = [feed.error for feed in errorfeeds]
        except TypeError, te:
            print te
            ### XXX display OPML Category error too

        text = list()
        if errorfeeds:
            text.append(_("Error:"))
            text += errortexts

        if text:
            t = "\n".join(text)
            self._tooltips.set_tip(self._widget,t,t)
            self._tooltips.enable()
            self._widget.show()
        else:
            self._tooltips.disable()
            self._widget.hide()
        return

    def hide(self):
        self._widget.hide()

class MenuFeedPropsPresenter(MVP.BasicPresenter):
    # FIXME 
    def set_sensitive(self, s):
        self._view.set_sensitive(s)
        return

class ToolbarView(MVP.WidgetView):
    """ Widget: gtk.Toolbar"""
    GCONF_DESKTOP_INTERFACE = "/desktop/gnome/interface"

    def _initialize(self):
        client = gconf.client_get_default()
        if not client.dir_exists(self.GCONF_DESKTOP_INTERFACE):
            return
        client.add_dir(self.GCONF_DESKTOP_INTERFACE,
                       gconf.CLIENT_PRELOAD_NONE)
        client.notify_add(self.GCONF_DESKTOP_INTERFACE+"/toolbar_style",
                          self._toolbar_style_changed)
        self._widget.set_tooltips(True)

    def _toolbar_style_changed(self, client, notify_id, entry, *args):
        value = entry.value.get_string()
        self._presenter.style_changed(value)

    def set_style(self, style):
        self._widget.set_style(style)

    def get_style(self):
        client = gconf.client_get_default()
        current_style = client.get_string(self.GCONF_DESKTOP_INTERFACE+"/toolbar_style")
        return current_style

class ToolbarPresenter(MVP.BasicPresenter):
    STYLES = {'both':gtk.TOOLBAR_BOTH,
              'text':gtk.TOOLBAR_TEXT,
              'icons':gtk.TOOLBAR_ICONS,
              'both-horiz':gtk.TOOLBAR_BOTH_HORIZ}

    def _initialize(self):
        style = self._view.get_style()
        if style in self.STYLES.keys():
            self._view.set_style(self.STYLES[style])
        return

    def style_changed(self, value):
        if value in self.STYLES.keys():
            self._view.set_style(self.STYLES[value])
        return

class ApplicationPresenter(MVP.BasicPresenter):
    def _initialize(self):
        self._curr_category = None
        self._curr_feed = None
        self._curr_item = None
        self._init_widgets()
        self._init_presenters()
        self._view.present()

    def _init_widgets(self):
        widget_tree = self._view.get_widget_tree()
        self._itemlist_view_notebook = widget_tree.get_widget('mode_view_notebook')
        self._feedlist_view_notebook = widget_tree.get_widget('left_mode_view_notebook')

        item_view_container = widget_tree.get_widget('item_view_container')
        item_list_container = widget_tree.get_widget('item_list_container')

        parent_paned = item_view_container.get_parent()
        parent_parent_widget = parent_paned.get_parent()

        config = Config.get_instance()

        child_list = []
        layout = config.pane_layout
        if layout == 'vertical':
            new_paned = gtk.VPaned()
            child_list.append(item_list_container)
            child_list.append(item_view_container)
        elif layout == 'horizontal':
            new_paned = gtk.HPaned()
            child_list.append(item_view_container)
            child_list.append(item_list_container)
        else: # Use vertical layout as default
            new_paned = gtk.VPaned()
            child_list.append(item_list_container)
            child_list.append(item_view_container)

        for child in child_list:
            child.reparent(new_paned)

        parent_parent_widget.remove(parent_paned)
        parent_parent_widget.add(new_paned)

        new_paned.show()

    def _init_presenters(self):
        widget_tree = self._view.get_widget_tree()
        toolbar_presenter = ToolbarPresenter(view=ToolbarView(widget_tree.get_widget('toolbar_default')))
        self._error_presenter = ErrorPresenter(widget_tree.get_widget('statusbar_error_indicator'))

        self._item_view = ItemView(widget_tree.get_widget('item_view_container'))

        view = ItemListView(widget_tree.get_widget('item_selection_treeview'))
        self._itemlist_presenter = ItemListPresenter(view=view)
        view.add_selection_changed_listener(self._item_view)

        view  = FeedsView(widget_tree.get_widget('feed_selection_treeview'))
        self._feed_list_presenter = FeedsPresenter(view=view)
        view.add_selection_changed_listener(self._itemlist_presenter)
        view.add_selection_changed_listener(self._error_presenter)

        self._offline_presenter = OfflineToggle(widget_tree.get_widget('offline_toggle'))

        self._status_presenter = StatusPresenter(view = widget_tree.get_widget("main_statusbar"))
        self._menufp_presenter = MenuFeedPropsPresenter( view = widget_tree.get_widget('feed_information'))
        self._menufp_presenter.set_sensitive(False)
#        self._find_presenter = FindPresenter(view=FindView(widget_tree.get_widget("find_vbox")))
        return

    def copy_itemview_text_selection(self):
        helpers.set_clipboard_text(self._item_view.get_selected_text())

    def check_allocation(self, widget, event):
        config = Config.get_instance()
        def check_size((width, height, widget)):
            if width == widget.allocation.width and height == widget.allocation.height:
                config.main_window_size = (width, height)
        if event.width != widget.allocation.width or event.height != widget.allocation.height:
            gobject.timeout_add(1000, check_size, (
                (event.width, event.height, widget)))

    def check_main_pane_position(self, widget):
        config = Config.get_instance()
        def check_position((position, widget)):
            if position == widget.get_position():
                config.main_pane_position = position
        pos = widget.get_position()
        if pos != config.main_pane_position:
            gobject.timeout_add(1000, check_position, (pos, widget))

    def check_sub_pane_position(self, widget):
        config = Config.get_instance()
        def check_position((position, widget)):
            if position == widget.get_position():
                config.sub_pane_position = position
        pos = widget.get_position()
        if pos != config.sub_pane_position:
            gobject.timeout_add(1000, check_position, (pos, widget))

    def credits(self):
        return helpers.credits()

    def poll_all(self):
        if self._warn_if_offline():
            fclist = feeds.category_list
            self._poll_categories(fclist.all_categories)
        return

    def poll_current_category(self):
        if self._warn_if_offline():
            self._poll_categories([self._curr_category])
        return

    def poll_current_feed(self):
        if self._warn_if_offline():
            pm = PollManager.get_instance()
            pm.poll([self._curr_feed])
        return

    def mark_feed_as_read(self):
        self._curr_feed.mark_all_read()

    def mark_all_as_read(self):
        mmgr = MainloopManager.get_instance()
        flist = feeds.feedlist.flatten_list()
        mmgr.call_pending()
        for feed in flist:
            feed.mark_items_as_read()
            if feed is self._curr_feed:
                continue
            feed.unload_contents()
            mmgr.call_pending()

    def remove_selected_feed(self):
#        self._feed_list_presenter.remove_selected_feed()
        pass

    def show_search(self):
#        self._find_presenter.item_list.signal_connect(Event.ItemSelectionChangedSignal,
#                                                      self._item_view.item_selection_changed)
        self._item_view.display_empty_search()
        self._itemlist_view_notebook.set_current_page(1)
        self._feedlist_view_notebook.set_current_page(1)
        self._feedinfo_presenter.hide()

    def hide_search(self):
#        self._find_presenter.clear()
        self._itemlist_view_notebook.set_current_page(0)
        self._feedlist_view_notebook.set_current_page(0)
        self._feedinfo_presenter.show()
#        self._find_presenter.item_list.signal_disconnect(Event.ItemSelectionChangedSignal,
#                                                     self._item_view.item_selection_changed)


    def _poll_categories(self, fclist):
        pm = PollManager.get_instance()
        pm.poll_categories(fclist)
        return

    def _warn_if_offline(self):
        config = Config.get_instance()
        will_poll = False
        if config.offline:
            response = self._view.show_offline_dialog()
            if response == gtk.RESPONSE_OK:
                config.offline = not config.offline
                will_poll = True
        else:
            will_poll = True
        return will_poll

    def display_previous_feed(self, item = None):
        """
        Displays the feed before the current selected feed
        """
        self._feed_list_presenter.select_previous_feed()

    def display_next_feed(self, item=None):
        """
        Displays the feed after the current selected feed
        """
        self._feed_list_presenter.select_next_feed()
        return

    def display_next_unread_feed(self):
        """
        Displays the next feed with an unread item
        """
        self._feed_list_presenter.select_next_feed(with_unread=True)
        pass

    def display_previous_item(self, item=None):
        """
        Displays the item before the current selected item. If the item is the
        first item, scrolls to the previous feed
        """
        is_prev = self._itemlist_presenter.select_previous_item()
        if not is_prev:
            # TODO HACK - implement select_previous_feed(select_last=True) ...
            # ... to select previous feed's last item
            self._feed_list_presenter.select_previous_feed()
            self._itemlist_presenter.select_last_item()
            pass
        return

    def display_next_item(self, item=None):
        """
        Displays the item after the current selected item. If the item is the
        last item, selectes the next feed. If the current feed is the last
        feed in the list, it goes back and selects the first feed
        """
        is_next = self._itemlist_presenter.select_next_item()
        if not is_next:
            is_next_feed = self._feed_list_presenter.select_next_feed()
            if not is_next_feed:
                self._feed_list_presenter.select_firsteed_feed()
        return

    def scroll_or_display_next_unread_item(self, item=None):
        has_unread_item = False
        if not self._item_view.scroll_down():
            has_unread_item = self._itemlist_presenter.select_next_unread_item()
            if not has_unread_item:
                self._feed_list_presenter.select_next_feed(with_unread=True)
        return

    def show_preferences_dialog(self, parent):
        straw.preferences_show()

    def show_feed_properties(self, parent):
        straw.feed_properties_show(self._curr_feed)

    def quit(self):
        gtk.main_quit()
        ItemStore.get_instance().stop()
        return

    def _setup_filechooser_dialog(title, action, extra_widget_title):
        """
        Setup the file chooser dialog. This includes an extra widget (a combobox)
        to include the categories to import or export
        """
        dialog = gtk.FileChooserDialog(title, action=action,
                                       buttons=(gtk.STOCK_CANCEL,
                                                gtk.RESPONSE_CANCEL,
                                                gtk.STOCK_OK, gtk.RESPONSE_OK))
        category_list = feeds.category_list
        model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_PYOBJECT)
        combobox = gtk.ComboBox(model)
        celltitle = gtk.CellRendererText()
        combobox.pack_start(celltitle,False)
        combobox.add_attribute(celltitle, 'text', 0)

        # add 'All' Category
        it = model.append()
        model.set(it, 0, category_list.all_category.title,
                  1, category_list.all_category)

        # add user categories
        for category in category_list.user_categories:
            it = model.append()
            model.set(it, 0, category.title, 1, category)

        # ..
            combobox.set_active(0)
            label = gtk.Label(extra_widget_title)
            label.set_alignment(1.0,0.5)
            hbox = gtk.HBox(spacing=6)
            hbox.pack_start(label,True,True,0)
            hbox.pack_end(combobox,False,False,0)
            hbox.show_all()

        dialog.set_extra_widget(hbox)
        return (dialog, combobox)


    def import_subscriptions(parent):
        (dialog,combobox) = _setup_filechooser_dialog(_("Import Subscriptions"),
                                                             gtk.FILE_CHOOSER_ACTION_OPEN,
                                                             _("Add new subscriptions in:"))
        ffilter = gtk.FileFilter()
        ffilter.set_name(_("OPML Files Only"))
        ffilter.add_pattern("*.xml")
        ffilter.add_pattern("*.opml")
        dialog.add_filter(ffilter)
        dialog.set_transient_for(parent)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            filename = dialog.get_filename()
            model = combobox.get_model()
            cat = model[combobox.get_active()][1]
            dialog.hide()
            feeds.import_opml(filename, cat)
        dialog.destroy()


    def export_subscriptions(parent):
        (dialog,combobox) = _setup_filechooser_dialog(_("Export Subscriptions"),
                                                             gtk.FILE_CHOOSER_ACTION_SAVE,
                                                             _("Select category to export:"))
        def selection_changed(widget, dialog):
            model = widget.get_model()
            category = model[widget.get_active()][1]
            dialog.set_current_name("Straw-%s.xml" % category.title)

        combobox.connect('changed',
                         selection_changed,
                         dialog)
        selection_changed(combobox, dialog)
        dialog.set_transient_for(parent)
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            filename = dialog.get_filename()
            model = combobox.get_model()
            cat = model[combobox.get_active()][1]
            opml.export(cat.title, cat.feeds, filename)
        dialog.destroy()


class ApplicationView(MVP.WidgetView):
    """
    Widget: straw_main
    """
    def _initialize(self):
        self._config = Config.get_instance()
        self._initialize_dnd()
        self._initialize_window_updater()
        self._create_unmodified_accelerator_group()
        self._attach_unmodified_accelerator_group()
        self._initialize_window()
        self._find_toggled = False

    def _initialize_window(self):
        widget_tree = gtk.glade.get_widget_tree(self._widget)
        if self._config.window_maximized:
            self._widget.maximize()
        else:
            # we use resize here since configure-event seems to
            # overwrite the default size if we use set_default_size.
            self._widget.resize(*self._config.main_window_size)
        mmp = widget_tree.get_widget('main_main_pane')
        msp = widget_tree.get_widget('main_sub_pane')
        mmp.set_position(self._config.main_pane_position)
        msp.set_position(self._config.sub_pane_position)

    def _initialize_dnd(self):
        self._widget.drag_dest_set(
                gtk.DEST_DEFAULT_ALL,
                [('_NETSCAPE_URL', 0, 0), ('text/uri-list ', 0, 1),
                 ('x-url/http', 0, 2)],
                gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_MOVE)
        return

    def _initialize_window_updater(self):
        feedlist = feeds.feedlist
        #feedlist.signal_connect(Event.AllItemsReadSignal,
        #                        lambda signal: self._update_title(feedlist))
        #feedlist.signal_connect(Event.ItemReadSignal,
        #                        lambda signal: self._update_title(feedlist))
        #feedlist.signal_connect(Event.ItemsAddedSignal,
        #                        lambda signal: self._update_title(feedlist))
        #feedlist.signal_connect(Event.FeedsChangedSignal,
        #                        lambda signal: self._update_title(feedlist))
        return

    def _update_title(self, flist):
        uritems = urfeeds = 0
        sfeeds = "feeds"
        listfeeds = flist.flatten_list()
        for ur in [f.number_of_unread for f in listfeeds]:
            if ur:
                uritems += ur
                urfeeds += 1
        else:
            urfeeds = len(listfeeds)
        if urfeeds < 2:
            sfeeds = "feed"
        item_feed_map = {'uritems': uritems,
                         'urfeeds': urfeeds,
                         'fstring' : sfeeds}
        title = _('%(uritems)d unread in %(urfeeds)d %(fstring)s') % item_feed_map
        self._widget.set_title( title + " - %s" % straw.PACKAGE)
        return

    # We have a separate accelerator group for the unmodified and
    # shifted accelerators, that is, stuff like space, N, P, etc. This
    # is so that we can have the find pane work correctly
    def _create_unmodified_accelerator_group(self):
        xml = gtk.glade.get_widget_tree(self._widget)
        agroup = gtk.AccelGroup()
        accels = (('feed_mark_as_read', 'R', gtk.gdk.SHIFT_MASK),
                  ('feed_mark_all_as_read', 'A', gtk.gdk.SHIFT_MASK),
                  ('next_item', 'N', gtk.gdk.SHIFT_MASK),
                  ('next_unread_feed', ' ', 0),
                  ('previous_item', 'P', gtk.gdk.SHIFT_MASK))
        for widget_name, key, mask in accels:
            widget = xml.get_widget(widget_name)
            widget.add_accelerator("activate", agroup, ord(key), mask,
                                   gtk.ACCEL_VISIBLE)
        self._unmodified_accelerator_group = agroup

    def _attach_unmodified_accelerator_group(self):
        self._widget.add_accel_group(self._unmodified_accelerator_group)

    def _detach_unmodified_accelerator_group(self):
        self._widget.remove_accel_group(self._unmodified_accelerator_group)

    def _on_straw_main_destroy_event(self, *args):
        return self._presenter.quit()

    def _on_straw_main_delete_event(self, *args):
        return self._presenter.quit()

    def _on_straw_main_configure_event(self, widget, event, *args):
        if widget.window.get_state() is not gtk.gdk.WINDOW_STATE_MAXIMIZED:
            self._config.window_maximized = False
            self._presenter.check_allocation(widget, event)
        else:
            self._config.window_maximized = True
        return

    def _on_main_main_pane_size_allocate(self, widget, *args):
        self._presenter.check_main_pane_position(widget)

    def _on_main_sub_pane_size_allocate(self, widget, *args):
        self._presenter.check_sub_pane_position(widget)

    # Actions menu
    def _on_feed_subscribe_activate(self, *args):
        straw.subscribe_show(parent=self._widget)

    def _on_feed_unsubscribe_activate(self, *args):
        self._presenter.remove_selected_feed()

    def _on_subscription_import__activate(self, *args):
        self._presenter.import_subscriptions(self._widget)

    def _on_subscription_export_activate(self, *args):
        self._presenter.export_subscriptions(self._widget)

    def _on_feed_mark_as_read_activate(self, *args):
        self._presenter.mark_feed_as_read()

    def _on_feed_mark_all_as_read_activate(self, *args):
        self._presenter.mark_all_as_read()

    def _on_feed_refresh_selected_activate(self, *args):
        self._presenter.poll_current_feed()

    def _on_subscription_refresh_activate(self, *args):
        self._presenter.poll_all()

    def _on_quit_activate(self, *args):
        return self._presenter.quit()

    # Edit menu
    def _on_copy_activate(self, *args):
        self._presenter.copy_itemview_text_selection()


    def _on_find_activate(self, widget, *args):
        xml = gtk.glade.get_widget_tree(self._widget)
        menu_find = xml.get_widget('menu_find')
        accel_label = menu_find.get_child()

        if not self._find_toggled:
            self._presenter.show_search()
            self._detach_unmodified_accelerator_group()
            self._find_toggled = True

            # save the "Find..." stock text for later recovery
            self._old_label_text = accel_label.get_text()
            accel_label.set_text(_('Return to feed list...'))

        else:
            self._presenter.hide_search()
            self._attach_unmodified_accelerator_group()
            self._find_toggled = False
            accel_label.set_text(self._old_label_text)

    def _on_preferences_activate(self, *args):
        self._presenter.show_preferences_dialog(self._widget)

    def _on_feed_information_activate(self, *args):
        self._presenter.show_feed_properties(self._widget)

    # View menu
    def _on_previous_item_activate(self, *args):
        self._presenter.display_previous_item()

    def _on_next_item_activate(self, *args):
        self._presenter.display_next_item()

    def _on_next_feed_activate(self, *args):
        self._presenter.display_next_feed()

    def _on_previous_feed_activate(self, *args):
        self._presenter.display_previous_feed()

    def _on_next_unread_feed_activate(self, *args):
        self._presenter.display_next_unread_feed()

    def _on_scroll_unread_items_activate(self, *args):
        self._presenter.scroll_or_display_next_unread_item()

    # Help menu

    def _on_report_problem_activate(self, menuitem, *args):
        helpers.url_show("http://bugzilla.gnome.org/simple-bug-guide.cgi?product=straw")

    def _on_about_activate(self, menuitem, *args):
        widget = self._presenter.credits()
        widget.show()

    def _on_straw_main_drag_data_received(self, w, context,
                                          x, y, data, info, time):
        if data and data.format == 8:
            url = data.data.split("\n")[0]
            straw.subscribe_show("%s" % url, self._widget)
            context.finish(True, False, time)
        else:
            context.finish(False, False, time)

    # FIXME
    def show_offline_dialog(self):
        return helpers.report_offline_status(self._widget)

    def get_widget_tree(self):
        return gtk.glade.get_widget_tree(self._widget)

    def present(self):
        self._widget.present()

    def should_present(self):
        if self._widget.window.get_state() is not gtk.gdk.WINDOW_STATE_WITHDRAWN:
            self._widget.hide()
        else:
            self._widget.present()


class Application:
    def __init__(self):
        gnome.program_init(straw.PACKAGE, straw.VERSION)
        # initialize threading and environment
        gobject.threads_init()
        config = Config.get_instance()
        config.reload_css = os.getenv('STRAW_RELOAD_CSS') is not None

        # build tray status icon
        image = gtk.Image()
        iconfile = os.path.normpath(straw.STRAW_DATA_DIR + "/straw.png")
        pixbuf = gtk.gdk.pixbuf_new_from_file(iconfile)
        scaled_buf = pixbuf.scale_simple(16,16,gtk.gdk.INTERP_BILINEAR)
        image.set_from_pixbuf(scaled_buf)
        try:
            self.tray = gtk.status_icon_new_from_pixbuf(scaled_buf)
            self.tray.connect('activate', self._tray_clicked)
        except AttributeError:
            import egg
            import egg.trayicon
            self.tray = egg.trayicon.TrayIcon(straw.PACKAGE);
            self._eventbox = gtk.EventBox()
            self.tray.add(self._eventbox)
            self._eventbox.connect('button_press_event', self._tray_clicked)
            self._eventbox.connect("drag-data-received", self._on_drag_data_received)
            self._eventbox.drag_dest_set(
                gtk.DEST_DEFAULT_ALL,
                [('_NETSCAPE_URL', 0, 0),('text/uri-list ', 0, 1),('x-url/http', 0, 2)],
                gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_MOVE)
            self._eventbox.add(image)
            self.tray.show_all()

        self._tooltip = gtk.Tooltips()
        self.unread_count = 0
        # end build tray status icon

        feedlist = feeds.feedlist
        feed_categories = feeds.category_list

        if config.first_time:
            filepath = os.path.join(straw.STRAW_DATA_DIR, "default_subscriptions.opml")
            feeds.import_opml(filepath)
        else:
            feedlist.load_data()
        feed_categories.load_data()

        try:
            itemstore = ItemStore.get_instance()
        except ItemStore.ConvertException, ex:
            helpers.report_error(_("There was a problem while converting the database."),
                                 _("Straw will not behave as expected. You should probably quit now. " +
                                   "The exception has been saved to the file '%s'. Please see the Straw README for further instructions."
                                   ) % ex.reason)
            sys.exit()

        ImageCache.initialize()
        itemstore.start()


        xml = gtk.glade.XML(os.path.join(straw.STRAW_DATA_DIR,'straw.glade'), "straw_main", gettext.textdomain())
        window = xml.get_widget('straw_main')
        self._main_presenter = ApplicationPresenter(view =
                                                    ApplicationView(window))
        self._main_presenter.view.present()

        PollManager.get_instance().start_polling_loop()
         # set the default icon for the windows
        iconfile = os.path.join(straw.STRAW_DATA_DIR,"straw.png")
        gtk.window_set_default_icon(gtk.gdk.pixbuf_new_from_file(iconfile))

        straw.start_services()

    def mainloop(self):
        gtk.main()
        return

    def _update(self,flist):
        uritems = urfeeds = 0
        for ur in [f.number_of_unread for f in flist.flatten_list()]:
            if ur:
                uritems += ur
                urfeeds += 1
        if uritems == self.unread_count:
            return
        self.unread_count = uritems
        if uritems:
            self._tooltip.set_tip(self.tray, _("%d new items")%uritems)
            self.tray.show_all()
        else:
            self.tray.hide()
        return


    def _tray_clicked(self, widget, event=None):
        self._main_presenter.view.should_present()
        if event and not (event.button == 1):
            return
        self._main_presenter.scroll_or_display_next_unread_item()

    def _on_drag_data_received(self, widget, context, x, y, data, info, timestamp):
        if data and data.format == 8:
            url = data.data.split("\n")[0]
            straw.subscribe_show(url="%s" % url)
            context.finish(True, False, timestamp)
        else:
            context.finish(False, False, timestamp)
        return
