""" ParsedSummary.py

"""

__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

class ParsedSummary(object):
    __slots__ = ('title', 'link', 'description', 'last_build_date', 'contributors',
                 'managing_editor', 'web_master', 'language', 'items', 'copyright', 'creator')

    def __init__(self):
        self.items = []
        self.last_build_date = -1
        self.title = ""
        self.description = ""
        self.copyright = ""
        self.link = ""
        self.language = ""
        self.creator = ""
        self.contributors = list()

    def addItem(self, item):
        self.items.append(item)

