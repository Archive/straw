""" PollManager.py

Module for polling the feeds.
"""
__copyright__ = "Copyright (c) 2002-4 Juri Pakaste <juri@iki.fi>"
__license__ = """ GNU General Public License

Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import gobject
import Config
import feeds
from MainloopManager import MainloopManager
import NetworkConstants
import URLFetch
import SummaryParser
import time
import socket
import error

import straw
from straw import helpers

class PollStopper(gobject.GObject):

    __gsignals__ = {
        'stopped' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self, stopper, object):
        gobject.GObject.__init__(self)
        self._stopper = stopper
        self._object = object

    stopper = property(lambda self: self._stopper)
    object = property(lambda self: self._object)

    def stop(self):
        if self._stopper is not None:
            self._stopper()
            self._stopper = None
            self.emit_signal('stopped')

class PollManager:
    def __init__(self):
        config = Config.get_instance()
        self._feedlist = feeds.feedlist

        config.connect('refresh-changed', self.poll_changed)
        config.connect('offline-mode-changed', self.offline_changed)

        self._feedlist.connect('changed', self.feeds_changed)

        self._is_offline = config.offline
        self._poll_frequency = config.poll_frequency
        self._last_poll = config.last_poll
        self._feeds = self._feedlist.flatten_list()
        self._pollers = {}

    def poll_changed(self, config):
        """ called when global poll frequency has changed"""
        self._poll_frequency = config.poll_frequency

    def offline_changed(self, config):
        self._is_offline = config.offline

    def feeds_changed(self, *args):
        self._feedlist = feeds.feedlist
        self._feeds = self._feedlist.flatten_list()

    def start_polling_loop(self):
        mlmgr = MainloopManager.get_instance()
        mlmgr.set_repeating_timer(NetworkConstants.POLL_INTERVAL, self.maybe_poll)

    ##########################
    # Why both pollcontext and self._pollers?
    # self._pollers is there to make sure that there are no simultaneous
    # pollers on the same object. So the user doesn't accidentally start
    # a poller for an object being polled.
    # pollcontext is there so that we can avoid starting pollers many
    # times - even though they wouldn't overlap - on the same object within
    # one polling run.

    # because maybe_poll is used as an idle handler, it must always return
    # True or else it's removed from the handler list
    def maybe_poll(self):
        self._maybe_poll()

    def _maybe_poll(self):
        now = int(time.time())
        if self._is_offline:
            return True
        try:
            context = {}
            config_pf = self._poll_frequency
            feed_use_default = feeds.Feed.DEFAULT
            self.poll([feed for feed, timediff, fpf in
                       [(feed, now - feed.last_poll, feed.poll_frequency)
                        for feed in self._feeds]
                       if ((timediff > fpf > 0) or
                           (fpf == feed_use_default and
                            timediff > config_pf > 0))], context)


            self.poll_categories([cat for cat, sub, timediff in
            [(cat, cat.subscription, now - cat.subscription.last_poll)
             for cat in feeds.category_list.all_categories
             if cat.subscription is not None]
            if ((timediff > sub.frequency > 0) or
                (sub.frequency == sub.REFRESH_DEFAULT and
                 timediff > config_pf > 0))], context, False)
        except:
            error.log_exc("Caught an exception while polling")
        return True

    def poll(self, obj, pollcontext = None):
        """ obj must be a list of Feed objects"""
        if pollcontext is None:
            pollcontext = {}
        stoppers = []
        for f in obj:
            sf = None
            if not self._pollers.has_key(f) and not pollcontext.has_key(f):
                self._pollers[f] = True
                pollcontext[f] = True
                sf = FeedPoller(f).poll()
            stoppers.append(sf)
        return stoppers

    def poll_categories(self, cats, pollcontext = None, feeds = True):
        """Polls categories and, if feeds is True, the feeds
        associated with them.  Returns a list of list containing
        PollStopper objects.  poll_categories([c1, c2, c3]) -> [[sc1,
        sc1f1, sc1f2 ...], [sc2, sc2f1, sc2f2 ...], ...] where cN is a
        category, scN is a PollStopper for cN, scNfM is a stopper for
        a feed in cN.  If no polling was started for that cN or cNfM,
        in its place is None.  """
        stoppers = []
        if pollcontext is None:
            pollcontext = {}
        for c in cats:
            category_stoppers = []
            sc = None
            if c.subscription is not None and not self._pollers.has_key(c) and not pollcontext.has_key(c):
                self._pollers[c] = True
                pollcontext[c] = True
                sc = CategoryPoller(c, pollcontext).poll()
            category_stoppers.append(sc)
            if feeds:
                for f in c.feeds:
                    sf = None
                    if not self._pollers.has_key(f) and not pollcontext.has_key(f):
                        self._pollers[f] = True
                        pollcontext[f] = True
                        sf = FeedPoller(f).poll(), f
                    category_stoppers.append(sf)
            stoppers.append(category_stoppers)
        return stoppers

    def poll_done(self, obj):
        if self._pollers.has_key(obj):
            del self._pollers[obj]

class FeedPoller:
    def __init__(self, feed):
        self._feed = feed

    def poll(self):
        MainloopManager.get_instance().call_pending()

        url, user, pw = self._feed.access_info
        parsed = None
        config = Config.get_instance()
        self._feed.last_poll = int (time.time())
        ps = None
        try:
            try:
                stopper = URLFetch.get_instance().request(
                    url, self, user, pw, priority=NetworkConstants.PRIORITY_RSS)
                ps = PollStopper(stopper, self._feed)
            except Exception, e:
                error.log_exc("Caught an exception while polling")
                self.http_failed(e)
            else:
                self._feed.router.start_polling()
                self._feed.router.stopper = ps
        finally:
            return ps

    def http_results(self, status, data):
        MainloopManager.get_instance().call_pending()
        try:
            try:
                err = ""
                read_data = True
                #if status is None:
                #    err = _("No data")
                #elif status[1] == 304:
                #    self._feed.router.route_no_data()
                #    read_data = False
                #elif status[1] == 410 or status[1] == 404:
                #    err = _("Unable to find the feed (%s: %s)") % (
                #        status[1], status[2].strip())
                #elif status[1] == 401:
                #    err = _("Invalid username and password.")
                #elif status[1] > 299:
                #    err = _("Updating feed resulted in abnormal status '%s' (code %d)") % (
                #        status[2].strip(), status[1])

                (x, code, message) = status
                if code == 304:
                    self._feed.router.route_no_data()
                    read_data = False
                elif code > 299:
                    err = "%s" % message

                if err:
                    self._feed.router.set_error(err)
                elif read_data:
                    try:
                        parsed = SummaryParser.parse(data, self._feed)
                    except Exception, e:
                        error.log_exc("exception in summaryparser")
                        self._feed.router.set_error(
                            _("An error occurred while processing feed: %s") %
                            str(e))
                    else:
                        self._feed.router.route_all(parsed)
                straw.post_status_message(_("Updating %s done.") % self._feed.title)
            except Exception, ex:
                error.log_exc("error while parsing results")
                self._feed.router.set_error(str(ex))
        finally:
            get_instance().poll_done(self._feed)

    def http_failed(self, exception):
        try:
            if isinstance(exception, socket.error):
                self._feed.router.route_no_data()
            else:
                self._feed.router.set_error(str(exception))
            straw.post_status_message(_("Updating %s failed") % self._feed.title)
            self._feed.poll_done()
        finally:
            get_instance().poll_done(self._feed)

    def http_permanent_redirect(self, location):
        # XXX 
        (oldloc, u, p) = self._feed.access_info
        self._feed.access_info = (location, u, p)

    def operation_stopped(self):
        self._feed.router.route_no_data()
        self._feed.poll_done()
        get_instance().poll_done(self._feed)

class CategoryPoller:
    def __init__(self, category, pollcontext):
        self._category = category
        self._pollcontext = pollcontext

    def poll(self):
        sub = self._category.subscription
        if sub is None or sub.location is None or len(sub.location) == 0:
            return None
        sub.last_poll = int(time.time())
        ps = None
        try:
            try:
                stopper = URLFetch.get_instance().request(
                    sub.location, self, sub.username, sub.password, priority=NetworkConstants.PRIORITY_RSS)
                ps = PollStopper(stopper, self._category)
            except Exception, e:
                error.log_exc("Caught an exception while polling category")
        finally:
            return ps

    def http_results(self, status,  data):
        sub = self._category.subscription
        try:
            try:
                err = None
                (x, code, message) = status
                if code == 304:
                    self._feed.router.route_no_data()
                    read_data = False
                elif code > 299:
                    err = "%s" % message

                if err is not None:
                    self._category.subscription.error = err
                else:
                    sub.parse(data)
                    old_feeds = self._category.feeds
                    self._category.read_contents_from_subscription()
                    common, inold, innew = helpers.listdiff(
                        old_feeds, self._category.feeds)
                    if len(innew) > 0:
                        get_instance().poll(innew, self._pollcontext)
            except Exception, e:
                self._category.subscription.error = str(e)
        finally:
            get_instance().poll_done(self._category)

    def http_failed(self, exception):
        self._category.subscription.error = str(exception)
        get_instance().poll_done(self._category)

    def http_permanent_redirect(self, location):
        error.logparam(locals(), "location")

    def operation_stopped(self):
        get_instance().poll_done(self._category)

pollmanager_instance = None

def get_instance():
    global pollmanager_instance
    if pollmanager_instance is None:
        pollmanager_instance = PollManager()
    return pollmanager_instance
