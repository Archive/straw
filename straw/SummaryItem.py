""" SummaryItem.py

"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """

import gobject
import time
import locale

class SummaryItem(gobject.GObject):
    __slots__ = ('title', 'link', 'description', 'guid', 'pub_date', 'source',
                 '_images', '_seen', '_id', 'feed', '_slots',
                 'fm_license', 'fm_changes', 'creator', 'license_urls',
                 '_searchable_fields', '_sticky', 'publication_name',
                 'publication_volume', 'publication_number',
                 'publication_section', 'publication_starting_page',
                 'guidislink', 'contributors', 'enclosures')

    _searchable_fields = ('title', 'description', 'fm_license', 'fm_changes',
                          'creator', 'contributors')

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        }

    def __init__(self, imageCache=None):
        gobject.GObject.__init__(self)
        self.imageCache = imageCache
        if not self.imageCache:
            import ImageCache
            self.imageCache = ImageCache.cache

        self.title = None
        self.link = None
        self.description = None
        self.guid = None
        self.guidislink = True
        self.pub_date = time.localtime()
        self.source = None
        self._images = {}
        self._seen = 0
        self._id = None
        self.feed = None
        self.fm_license = None
        self.fm_changes = None
        self.creator = None
        self.contributors = list()
        self.license_urls = list()
        self.publication_name = None
        self.publication_volume = None
        self.publication_number = None
        self.publication_section = None
        self.publication_starting_page = None
        self._sticky = 0
        self.enclosures = None

    def __eq__(self, item):
        r = item.title == self.title and item.description == self.description
        if r:
            return True
        elif item.guid and self.guid and item.guid == self.guid:
            return True
        return False

    @apply
    def seen():
        doc = ""
        def fget(self):
            return self._seen
        def fset(self, seen = True):
            if self._seen != seen:
                self._seen = seen
                self.emit('changed')
        return property(**locals())

    @apply
    def sticky():
        doc = ""
        def fget(self):
            return self._sticky
        def fset(self, sticky):
            if self._sticky != sticky:
                self._sticky = sticky
                self.emit('changed')
        return property(**locals())

    @apply
    def id():
        doc = "Item ID (internal)"
        def fget(self):
            return self._id
        def fset(self, iid):
            self._id = iid
        return property(**locals())

    def _add_image(self, image_name, restore):
        self.imageCache.add_refer(image_name, restore, self)
        self._images[image_name] = self.imageCache[image_name]

    def add_image(self, image_name):
        self._add_image(image_name, restore = False)

    def restore_image(self, image_name):
        self._add_image(image_name, restore = True)

    def set_image(self, image_name, image):
        self._images[image_name] = image

    def get_image(self, image_name):
        return self._images.get(image_name, None)

    def clear_images(self):
        self._images = {}

    def image_keys(self):
        return self._images.keys()

    def match(self, text):
        def try_field(field):
            return type(field) in (
                type(''), type(u'')) and field.lower().find(text.lower()) > -1
        for f in self._searchable_fields:
            try:
                if try_field(self.__getattribute__(f)):
                    return 1
            except AttributeError:
                pass
        return 0

    def clean_up(self):
        for image in self._images:
            self.imageCache.remove_refer(image)
