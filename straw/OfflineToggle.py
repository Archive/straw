""" OfflineToggle.py

"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """


import pygtk
pygtk.require('2.0')
import gtk
import os
import MVP
import Config

class OfflineView(MVP.WidgetView):
    """
    Model: Config instance
    Presenter: OfflinePresenter instance
    """
    def _initialize(self):
        self._tooltips = gtk.Tooltips()
        self._tooltips.enable()

    def _on_offline_toggle_toggled(self, *args):
        status = self._widget.get_active()
        if status != self._model.offline:
            self._model.offline = status

    def set_status(self, active):
        self._widget.set_active(active)

    def set_text(self, text):
        self._tooltips.set_tip(self._widget, text, text)

    def set_icon(self, icon):
        image = self._widget.get_child()
        image.set_from_stock(icon, gtk.ICON_SIZE_BUTTON)

class OfflinePresenter(MVP.BasicPresenter):
    """
    Model: Config instance
    View: OfflineView instance
    """

    def _initialize(self):
        self._attribs = {'online': (gtk.STOCK_CONNECT, _("Straw is currently online. Click to go offline.")),
                         'offline': (gtk.STOCK_DISCONNECT, _("Straw is currently offline. Click to go online."))}

        self._model.connect('offline-mode-changed', self._config_offline_changed)
        if self._model.offline:
            self._config_offline_changed(None)

    def _config_offline_changed(self, config):
        offline = self._model.offline
        if offline:
            icon, tip = self._attribs.get('offline')
        else:
            icon, tip = self._attribs.get('online')
        self._view.set_status(offline)
        self._view.set_text(tip)
        self._view.set_icon(icon)

class OfflineToggle:
    def __init__(self, widget):
        config = Config.get_instance()
        oview =  OfflineView(widget, model=config)
        self._presenter = OfflinePresenter(model=config, view=oview)

