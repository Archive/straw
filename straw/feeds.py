# Copyright (c) 2002-2004 Juri Pakaste <juri@iki.fi>
# Copyright (c) 2005-2007 Straw Contributors 
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program; if not, write to the
# Free Software Foundation, Inc., 59 Temple Place - Suite 330,
# Boston, MA 02111-1307, USA.

import locale, operator
import types
from gettext import gettext as _
from StringIO import StringIO

import gobject

from straw import ItemStore
from straw import Config
from straw import error
from straw import helpers
from straw import opml

def opml_import(filename, category=None):
    opmlstr = read(open(filename))
    listfeeds = [feed.access_info[0] for feed in feedlist]
    newitems = [feeds.Feed.create_new_feed(b.text, b.url) for b in opmlstr if b.url not in listfeeds]
    feedlist.extend(category, newitems)
    return

class FeedList(gobject.GObject):

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self, init_seq = []):
        gobject.GObject.__init__(self)
        self.feedlist = []
        self._loading = False

    def __iter__(self):
        return iter(self.feedlist)

    def load_data(self):
        def _load(feedlist, parent):
            for df in feedlist:
                if isinstance(df, list):
                    _load(df[1:], parent)
                else:
                    f = Feed.create_empty_feed()
                    f.undump(df)
                    self.append(parent, f)
        self._loading = True
        feedlist = Config.get_instance().feeds
        if feedlist:
            _load(feedlist, None)
        self._loading = False
        self.emit('changed')

        # these signals are forwarded so that listeners who just want to
        # listen for a specific event regardless of what feed it came from can
        # just connect to this feedlist instead of connecting to the
        # individual feeds.
        #ob.signal_connect(Event.AllItemsReadSignal, self._forward_signal)
        #ob.signal_connect(Event.ItemReadSignal, self._forward_signal)
        #ob.signal_connect(Event.ItemsAddedSignal, self._forward_signal)
        #ob.signal_connect(Event.FeedPolledSignal, self._forward_signal)
        #ob.signal_connect(Event.FeedStatusChangedSignal, self._forward_signal)
        #ob.signal_connect(Event.FeedErrorStatusChangedSignal, self._forward_signal)

    def __setitem__(self, key, value):
        self.feedlist.__setitem__(key, value)
        value.connect('changed', self.feed_detail_changed)
        self.save_feeds_and_notify(True)

    def extend(self, parent, values, from_sub=False):
        list.extend(self.feedlist, values)
        for f in values:
            f.parent = parent
            f.connect('changed', self.feed_detail_changed)
        self.save_feeds()

    def append(self, parent, value):
        self.feedlist.append(value)
        value.parent = parent
        value.connect('changed', self.feed_detail_changed)
        self.save_feeds()

    def insert(self, index, parent, value):
        self.feedlist.insert(index, value)
        value.parent = parent
        value.connect('changed', self.feed_detail_changed)
        self.save_feeds()

    def index(self, feed):
        return self.feedlist.index(feed)

    def reorder(self, move, delta):
        k = self.feedlist[:]
        move = list(move)
        move.sort()
        if delta > 0:
            move.reverse()
        if move[0] == 0 and delta < 0 or move[-1] == (len(self.feedlist) - 1) and delta > 0:
            return
        for m in move:
            k[m + delta], k[m] = k[m], k[m + delta]
        for i in range(len(k)):
            list.__setitem__(self.feedlist, i, k[i])
        self.save_feeds()

    def __delitem__(self, value):
        feed = self.feedlist[value]
        list.__delitem__(self.feedlist, value)
        feed.delete_all_items()
        self.save_feeds()

    def save_feeds(self):
        if not self._loading:
            config = Config.get_instance()
            config.feeds = [f.dump() for f in self.feedlist]
        return

    def feed_detail_changed(self, feed):
        self.save_feeds()
#        self.emit('changed') # XXXX send the feed as well?

    def _sort_dsu(self, seq):
        aux_list = [(x.title, x) for x in seq]
        aux_list.sort(lambda a,b:locale.strcoll(a[0],b[0]))
        return [x[1] for x in aux_list]

    def sort(self, indices = None):
        if not indices or len(indices) == 1:
            self[:] = self._sort_dsu(self)
        else:
            items = self._sort_dsu(indices)
            for i,x in enumerate(items):
                list.__setitem__(self, indices[i], items[i])
        self.save_feeds()
#        self.emit('changed')

    def __hash__(self):
        h = 0
        for item in self.feedlist:
            h ^= hash(item)
        return h

    def get_feed_with_id(self, id):
        for f in self.flatten_list():
            if f.id == id:
                return f
        return None

    def flatten_list(self, ob=None):
        if ob is None:
            ob = self.feedlist
        l = []
        for o in ob:
            if isinstance(o, list):
                l = l + self.flatten_list(o)
            else:
                l.append(o)
        return l

feedlist_instance = None

def get_feedlist_instance():
    global feedlist_instance
    if feedlist_instance is None:
        feedlist_instance = FeedList()
    return feedlist_instance

feedlist = get_feedlist_instance()

class IdleState(object):
    ''' state for idle or normal operation '''
    def __init__(self):
        filename = os.path.join(straw.STRAW_DATA_DIR, 'feed.png')
        self.icon = gtk.gdk.pixbuf_new_from_file(filename)

    @property
    def icon(self):
        return self.icon

    @property
    def mesg(self):
        return None

class PollingState(object):
    ''' state when feed is polling '''
    def __init__(self):
        self._icon = gtk.Image()
        self._icon.set_from_stock(gtk.STOCK_EXECUTE, gtk.ICON_SIZE_MENU)

    @property
    def icon(self):
        return self._icon.get_pixbuf()

    @property
    def mesg(self):
        return None

class ErrorState(object):
    ''' state when feed has errors '''

    def __init__(self, mesg):
        self._icon = gtk.Image()
        self._icon.set_from_stock(gtk.STOCK_DIALOG_ERROR, gtk.ICON_SIZE_MENU)
        self._mesg = mesg

    @property
    def icon(self):
        return self._icon.get_pixbuf()

    @property
    def mesg(self):
        return self._mesg

class Feed(gobject.GObject):
    "A Feed object stores information set by user about a RSS feed."

    DEFAULT = -1
    STATUS_IDLE = 0
    STATUS_POLLING = 1

    __slots__ = ('_title', '_location', '_username', '_password', '_parsed',
                 '__save_fields', '_items', '_slots',
                 '_id', '_channel_description',
                 '_channel_title', '_channel_link', '_channel_copyright',
                 'channel_lbd', 'channel_editor', 'channel_webmaster',
                 'channel_creator','_error', '_process_status', 'router', 'sticky', '_parent',
                 '_items_stored', '_poll_freq', '_last_poll','_n_items_unread')

    __save_fields = (('_title', ""), ('_location', ""), ('_username', ""),
                     ('_password', ""), ('_id', ""),
                     ('_channel_description', ""), ('_channel_title', ""),
                     ('_channel_link', ""), ('_channel_copyright', ""),
                     ('channel_creator', ""), ('_error', None),
                     ('_items_stored', DEFAULT),
                     ('_poll_freq', DEFAULT),
                     ('_last_poll', 0),
                     ('_n_items_unread',0))


    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'poll-done' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'items-added' :(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                           (gobject.TYPE_PYOBJECT,)),
        'items-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                        (gobject.TYPE_PYOBJECT,)),
        'items-deleted' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                          (gobject.TYPE_PYOBJECT,))
        }


    # use one of the factory functions below instead of this directly
    def __init__(self, title="", location="", username="", password=""):
        import FeedDataRouter
        gobject.GObject.__init__(self)
        self._title = title
        self._channel_description = ""
        self._channel_title = ""
        self._channel_link = ""
        self._channel_copyright = ""
        self.channel_lbd = None
        self.channel_editor = ""
        self.channel_webmaster = ""
        self.channel_creator = ""
        self._location = location
        self._username = username
        self._password = password
        self._parsed = None
        self._error = None
        self._n_items_unread = 0
        self._process_status = self.STATUS_IDLE
        self.router = FeedDataRouter.FeedDataRouter(self)
        self._parent = None
        self._items_stored = Feed.DEFAULT
        self._poll_freq = Feed.DEFAULT
        self._last_poll = 0

        self.config = Config.get_instance()
        # XXX move this to subscriptions
        self.config.connect('item-order-changed', self.item_order_changed_cb)
        self.config.connect('item-stored-changed', self.item_stored_changed_cb)
        self.item_order_reverse = self.config.item_order
        self.item_stored_num = self.config.number_of_items_stored
        self._items = FifoCache(num_entries=Feed.DEFAULT)
        self._items_loaded = False
        return

    def __str__(self):
        return "Feed '%s' from %s" % (self._title, self._location)

    @property
    def id(self):
        return self._id

    @apply
    def parsed():
        doc = "A ParsedSummary object generated from the summary file"
        def fget(self):
            return self._parsed
        def fset(self, parsed):
            self._parsed = parsed
        return property(**locals())

    @apply
    def title():
        doc = "The title of this Feed (as defined by user)"
        def fget(self):
            text = ''
            if self._title:
                text = self._title
            return text
        def fset(self, title):
            if self._title != title:
                self._title = title
                self.emit('changed')
        return property(**locals())

    @apply
    def access_info():
        doc = "A tuple of location, username, password"
        def fget(self):
            return (self._location, self._username, self._password)
        def fset(self, (location,username,password)):
            self._location = location
            self._username = username
            self._password = password
            self.emit('changed')
        return property(**locals())

    @apply
    def location():
        doc = ""
        def fget(self):
            return self._location
        def fset(self, location):
            if self._location != location:
                self._location = location
                self.emit('changed')
        return property(**locals())

    @apply
    def channel_title():
        doc = ""
        def fget(self):
            text = ''
            if self._channel_title:
                text = self._channel_title
            return text
        def fset(self, t):
            changed = self._channel_title != t
            self._channel_title = t
            if changed:
                self.emit('changed')
        return property(**locals())

    @apply
    def channel_description():
        doc = ""
        def fget(self):
            text = ''
            if self._channel_description:
                text = self._channel_description
            return text
        def fset(self, t):
            changed = self._channel_description != t
            self._channel_description = t
            if changed:
                self.emit('changed')
        return property(**locals())

    @apply
    def channel_link():
        doc = ""
        def fget(self):
            return self._channel_link
        def fset(self, t):
            changed = self._channel_link != t
            self._channel_link = t
            if changed:
                self.emit('changed')
        return property(**locals())

    @apply
    def channel_copyright():
        doc = ""
        def fget(self):
            return self._channel_copyright
        def fset(self, t):
            changed = self._channel_copyright != t
            self._channel_copyright = t
            if changed:
                self.emit('changed')
        return property(**locals())

    @apply
    def number_of_items_stored():
        doc = ""
        def fget(self):
            return self._items_stored
        def fset(self, num=None):
            if self._items_stored != num:
                self._items_stored = num
        return property(**locals())

    @apply
    def poll_frequency():
        doc = ""
        def fget(self):
            return self._poll_freq
        def fset(self, freq):
            if self._poll_freq != freq:
                self._poll_freq = freq
        return property(**locals())

    @apply
    def last_poll():
        doc = ""
        def fget(self):
            return self._last_poll
        def fset(self, time):
            if self._last_poll != time:
                self._last_poll = time
        return property(**locals())

    @apply
    def number_of_unread():
        doc = "the number of unread items for this feed"
        def fget(self):
            return self._n_items_unread
        def fset(self, n):
            self._n_items_unread = n
            self.emit('changed')
        return property(**locals())

    @apply
    def error():
        doc = ""
        def fget(self):
            return self._error
        def fset(self, error):
            if self._error != error:
                self._error = error
                self.emit('changed')
        return property(**locals())

    @apply
    def process_status():
        doc = ""
        def fget(self):
            return self._process_status
        def fset(self, status):
            if status != self._process_status:
                self._process_status = status
                self.emit('changed')
        return property(**locals())

    @apply
    def parent():
        doc = ""
        def fget(self):
            return self._parent
        def fset(self, parent):
            self._parent = parent
        return property(**locals())

    @property
    def next_refresh(self):
        """ return the feed's next refresh (time)"""
        nr = None
        if self._poll_freq == self.DEFAULT:
            increment = self.config.poll_frequency
        else:
            increment = self._poll_freq
        if increment > 0:
            nr = self.last_poll + increment
        return nr

    def dump(self):
        fl = {}
        for f, default in self.__save_fields:
            fl[f] = self.__getattribute__(f)
        return fl

    def undump(self, dict):
        for f, default in self.__save_fields:
            self.__setattr__(f, dict.get(f, default))
        return

    def poll_done(self):
        self.emit('poll-done')

    def item_order_changed_cb(self, config):
        self.item_order_reverse = config.item_order

    def item_stored_changed_cb(self, config):
        self.item_stored_num = config.number_of_items_stored

    def get_cutpoint(self):
        cutpoint = self.number_of_items_stored
        if cutpoint == Feed.DEFAULT:
            cutpoint = self.item_stored_num
        return cutpoint


    def add_items(self, items):
        if not self._items_loaded: self.load_contents()
        items = sorted(items, key=operator.attrgetter('pub_date'),
                       reverse=self.item_order_reverse)
        self._items.set_number_of_entries(self.get_cutpoint())
        maxid = 0
        if self._items:
            maxid = reduce(max, [item.id for item in self._items.itervalues()])
        newitems = filter(lambda x: x not in self._items.itervalues(), items)
        for item in newitems:
            maxid += 1
            item.id = maxid
            item.feed = self
            item.connect('changed', self.item_changed_cb)
            self._items[item.id] = item  ## XXX pruned items don't get clean up properly
        self.number_of_unread = len(filter(lambda x: not x.seen, self._items.itervalues()))
        print self._items.keys()
        self.emit('items-added', newitems)

    def restore_items(self, items):
        items = sorted(items, key=operator.attrgetter('pub_date'),
                       reverse=self.item_order_reverse)
        cutpoint = self.get_cutpoint()
        self._items.set_number_of_entries(cutpoint)
        olditems = []
        for idx, item in enumerate(items):
            if not item.sticky and idx >= cutpoint:
                item.clean_up()
                olditems.append(item)
                continue
            item.feed = self
            item.connect('changed', self.item_changed_cb)
            self._items[item.id] = item
        self.number_of_unread = len(filter(lambda x: not x.seen, self._items.itervalues()))
        if olditems: self.emit('items-deleted', olditems)
        print self._items.keys()
        return

    def item_changed_cb(self, item):
        self.number_of_unread = len(filter(lambda x: not x.seen, self._items.itervalues()))
        self.emit('items-changed', [item])

    def delete_all_items(self):
        self._items.clear()
        self.emit('items-deleted', self._items.values())

    @property
    def items(self):
        if not self._items_loaded:
            self.load_contents()
        return self._items.values()

    def mark_items_as_read(self, items=None):
        def mark(item): item.seen = True
        unread = [item for item in self._items.itervalues() if not item.seen]
        map(mark, unread)
        self.emit('items-changed', unread)

    def load_contents(self):
        if self._items_loaded:
            return False
        itemstore = ItemStore.get_instance()
        items = itemstore.read_feed_items(self)
        print "feed.load_contents->items: ", len(items)
        if items:
            self.restore_items(items)
            self._items_loaded = True
        return self._items_loaded

    def unload_contents(self):
        if not self._items_loaded:
            return
        self._items.clear()
        self._items_loaded = False

    @classmethod
    def create_new_feed(klass, title, location="", username="", password=""):
        f = klass()
        f._title = title
        f._location = location
        f._id = Config.get_instance().next_feed_id_seq()
        f._username = username
        f._password = password
        return f

    @classmethod
    def create_empty_feed(klass):
        f = klass()
        return f


import UserDict
from collections import deque

class FifoCache(object, UserDict.DictMixin):
    ''' A mapping that remembers the last 'num_entries' items that were set '''

    def __init__(self, num_entries, dct=()):
        self.num_entries = num_entries
        self.dct = dict(dct)
        self.lst = deque()

    def __repr__(self):
        return '%r(%r,%r)' % (
            self.__class__.__name__, self.num_entries, self.dct)

    def copy(self):
        return self.__class__(self.num_entries, self.dct)

    def keys(self):
        return list(self.lst)

    def __getitem__(self, key):
        return self.dct[key]

    def __setitem__(self, key, value):
        dct = self.dct
        lst = self.lst
        if key in dct:
            self.remove_from_deque(lst, key)
        dct[key] = value
        lst.append(key)
        if len(lst) > self.num_entries:
            del dct[lst.popleft()]

    def __delitem__(self, key):
        self.dct.pop(key)
        self.remove_from_deque(self.lst, key)

    # a method explicitly defined only as an optimization
    def __contains__(self, item):
        return item in self.dct

    has_key = __contains__

    def remove_from_deque(self, d, x):
        for i, v in enumerate(d):
            if v == x:
                del d[i]
            return
        raise ValueError, '%r not in %r' % (x,d)

    def set_number_of_entries(self, num):
        self.num_entries = num
        while len(self.lst) >  num:
            self.dct.pop(self.lst.popleft())

PSEUDO_ALL_KEY = 'ALL'
PSEUDO_UNCATEGORIZED_KEY = 'UNCATEGORIZED'

class FeedCategoryList(gobject.GObject):

    __gsignals__ = {
        'added' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'deleted' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_PYOBJECT,)),
        'category-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'pseudo-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self):
        gobject.GObject.__init__(self)
        # have to define this here so the titles can be translated
        PSEUDO_TITLES = {PSEUDO_ALL_KEY: _('All'),
                         PSEUDO_UNCATEGORIZED_KEY: _('Feeds')}
        self._all_category = PseudoCategory(PSEUDO_TITLES[PSEUDO_ALL_KEY],
                                            PSEUDO_ALL_KEY)
        self._un_category = PseudoCategory(
            PSEUDO_TITLES[PSEUDO_UNCATEGORIZED_KEY], PSEUDO_UNCATEGORIZED_KEY)
        self._user_categories = []
        self._pseudo_categories = (self._all_category, self._un_category)
        self._loading = False
        self._feedlist = get_feedlist_instance()

    def load_data(self):
        """Loads categories from config.
        Data format: [[{'title': '', 'subscription': {}, 'pseudo': pseudo key},
        {'id', feed_id1, 'from_subscription': bool} ...], ...]
        """
        cats = Config.get_instance().categories or []
        categorized = {}
        for c in cats:
            head = c[0]
            tail = c[1:]
            pseudo_key = head.get('pseudo', None)
            if pseudo_key == PSEUDO_ALL_KEY:
                fc = self.all_category
            elif pseudo_key == PSEUDO_UNCATEGORIZED_KEY:
                fc = self.un_category
            else:
                fc = FeedCategory(head['title'])
                sub = head.get('subscription', None)
                if sub is not None:
                    fc.subscription = undump_subscription(sub)
            for f in tail:
                # backwards compatibility for stuff saved with versions <= 0.23
                if type(f) is int:
                    fid = f
                    from_sub = False
                else:
                    fid = f['id']
                    from_sub = f['from_subscription']
                feed = self._feedlist.get_feed_with_id(fid)
                if feed and not pseudo_key: # we deal with pseudos later
                    if feed in fc.feeds:
                        error.log("%s (%d) was already in %s, skipping" % (str(feed), fid, str(fc)))
                        continue
                    fc.append_feed(feed, from_sub)
                    categorized[feed] = True
            # User categories: connect pseudos later
            if not pseudo_key:
                fc.connect('changed', self.category_changed)
                self._user_categories.append(fc)
        # just in case we've missed any feeds, go through the list
        # and add to the pseudocategories. cache the feed list of all_category
        # so we don't get a function call (and a list comprehension loop
        # inside it) on each feed. it should be ok here, there are no
        # duplicates in feedlist. right?
        pseudos_changed = False
        all_feeds = self.all_category.feeds
        for f in self._feedlist:
            if f not in all_feeds:
                self.all_category.append_feed(f, False)
                pseudos_changed = True
            uf =  categorized.get(f, None)
            if not uf:
                self.un_category.append_feed(f, False)
                pseudos_changed = True
        if pseudos_changed:
           self.save_data()
        for cat in self.pseudo_categories:
            cat.connect('changed', self.pseudo_category_changed)

    def save_data(self):
        Config.get_instance().categories = [
            cat.dump() for cat in self]

    def pseudo_category_changed(self, category, *args):
        self.save_data()
        self.emit('pseudo-changed')

    def category_changed(self, signal):
        if signal.feed is not None:
            uncategorized = True
            for cat in self.user_categories:
                if signal.feed in cat.feeds:
                    uncategorized = False
                    break
            if uncategorized:
                self.un_category.append_feed(signal.feed, False)
            else:
                try:
                    self.un_category.remove_feed(signal.feed)
                except ValueError:
                    pass
        self.save_data()
        self.emit('category-changed')

    def remove_feed(self, feed):
        for c in self:
            try:
                c.remove_feed(feed)
                del self._feedlist[feed]
            except ValueError:
                pass

    def append_feed(self, feed, category=None, index=None):
        self._feedlist.append(category, feed)
        if category and category not in self.pseudo_categories:
            if index:
                category.insert_feed(index, feed, False)
            else:
                category.append_feed(feed, False)
        else:
            print "uncategory append"
            self.un_category.append_feed(feed, False)
        print "all category append"
        self.all_category.append_feed(feed, False)

    def import_feeds(self, feeds, category=None, from_sub=False):
        self._feedlist.extend(category, feeds)
        if category and category not in self.pseudo_categories:
            category.extend_feed(feeds, from_sub)
        else:
            print 'feed imported ', feeds
            self.un_category.extend_feed(feeds, from_sub)
        self.all_category.extend_feed(feeds, from_sub)

    @property
    def user_categories(self):
        return self._user_categories

    @property
    def pseudo_categories(self):
        return self._pseudo_categories

    @property
    def all_categories(self):
        return self.pseudo_categories + tuple(self.user_categories)

    @property
    def all_category(self):
        return self._all_category

    @property
    def un_category(self):
        return self._un_category

    class CategoryIterator:
        def __init__(self, fclist):
            self._fclist = fclist
            self._index = -1

        def __iter__(self):
            return self

        def _next(self):
            self._index += 1
            i = self._index
            uclen = len(self._fclist.user_categories)
            if i < uclen:
                return self._fclist.user_categories[i]
            elif i < uclen + len(self._fclist.pseudo_categories):
                return self._fclist.pseudo_categories[i - uclen]
            else:
                raise StopIteration

        def next(self):
            v = self._next()
            return v

    def __iter__(self):
        return self.CategoryIterator(self)

    def add_category(self, category):
        category.connect('changed', self.category_changed)
        self._user_categories.append(category)
        auxlist = [(x.title.lower(),x) for x in self._user_categories]
        auxlist.sort()
        self._user_categories = [x[1] for x in auxlist]
        self.save_data()
        self.emit('added', category)

    def remove_category(self, category):
        for feed in category.feeds:
            category.remove_feed(feed)
        self._user_categories.remove(category)
        self.save_data()
        self.emit('deleted', category)

# It might be good to have a superclass FeedCategorySubscription or something
# so we could support different formats. However, I don't know of any other
# relevant format used for this purpose, so that can be done later if needed.
# Of course, they could also just implement the same interface.
class OPMLCategorySubscription(gobject.GObject):
    REFRESH_DEFAULT = -1

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ()),
        'updated' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
        }

    def __init__(self, location=None):
        gobject.GObject.__init__(self)
        self._location = location
        self._username = None
        self._password = None
        self._contents = None
        self._frequency = OPMLCategorySubscription.REFRESH_DEFAULT
        self._last_poll = 0
        self._error = None

    @apply
    def location():
        doc = ""
        def fget(self):
            return self._location
        def fset(self, location):
            self._location = location
            self.emit('changed')
        return property(**locals())

    @apply
    def username():
        doc = ""
        def fget(self):
            return self._username
        def fset(self, username):
            self._username = username
            self.emit('changed')
        return property(**locals())

    @apply
    def password():
        doc = ""
        def fget(self):
            return self._password
        def fset(self, password):
            self._password = password
            self.emit('changed')
        return property(**locals())

    @apply
    def frequency():
        doc = ""
        def fget(self):
            return self._frequency
        def fset(self, freq):
            self._frequency = freq
            self.emit('changed')
        return property(**locals())

    @apply
    def last_poll():
        doc = ""
        def fget(self):
            return self._last_poll
        def fset(self, last_poll):
            self._last_poll = last_poll
            self.emit('changed')
        return property(**locals())

    @apply
    def error():
        doc = ""
        def fget(self):
            return self._error
        def fset(self, error):
            self._error = error
            self.emit('changed')
        return property(**locals())

    def parse(self, data):
        datastream = StringIO(data)
        entries = opml.read(datastream)
        contents = [(e.url, e.text) for e in entries]
        updated = contents == self._contents
        self._contents = contents
        if updated:
            self.emit('updated')
        return

    @property
    def contents(self):
        return self._contents

    @classmethod
    def undump(klass, dictionary):
        sub = klass()
        sub.location = dictionary.get('location')
        sub.username = dictionary.get('username')
        sub.password = dictionary.get('password')
        sub.frequency = dictionary.get(
            'frequency', OPMLCategorySubscription.REFRESH_DEFAULT)
        sub.last_poll = dictionary.get('last_poll', 0)
        sub.error = dictionary.get('error')
        return sub

    def dump(self):
        return {'type': 'opml',
                'location': self.location,
                'username': self.username,
                'password': self.password,
                'frequency': self.frequency,
                'last_poll': self.last_poll,
                'error': self.error}

def undump_subscription(dictionary):
    try:
        if dictionary.get('type') == 'opml':
            return OPMLCategorySubscription.undump(dictionary)
    except Exception, e:
        error.log("exception while undumping subscription: " + str(e))
        raise

class CategoryMember(object):
    def __init__(self, feed=None, from_sub=False):
        self._feed = feed
        self._from_subscription = from_sub

    @apply
    def feed():
        doc = ""
        def fget(self):
            return self._feed
        def fset(self, feed):
            self._feed = feed
        return property(**locals())

    @apply
    def from_subscription():
        doc = ""
        def fget(self):
            return self._from_subscription
        def fset(self, p):
            self._from_subscription = p
        return property(**locals())

class FeedCategory(gobject.GObject):

    __gsignals__ = {
        'changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,()),
        'feed-added':(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                      (gobject.TYPE_PYOBJECT,)),
        'feed-removed':(gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE,
                        (gobject.TYPE_PYOBJECT,))
        }

    def __init__(self, title=""):
        gobject.GObject.__init__(self)
        self.feedlist = []
        self._title = title
        self._subscription = None

    @apply
    def title():
        doc = ""
        def fget(self):
            return self._title
        def fset(self, title):
            self._title = title
            self.emit('changed')
        return property(**locals())

    @apply
    def subscription():
        doc = ""
        def fget(self):
            return self._subscription
        def fset(self, sub):
            self._subscription = sub
            self._subscription.connect('changed', self._subscription_changed)
            self._subscription.connect('updated', self._subscription_contents_updated)
            self.emit('changed')
        return property(**locals())

    def read_contents_from_subscription(self):
        if self.subscription is None:
            return
        subfeeds = self.subscription.contents
        sfdict = dict(subfeeds)
        feedlist = get_feedlist_instance()
        current = dict([(feed.location, feed) for feed in self.feeds])
        allfeeds = dict([(feed.location, feed) for feed in feedlist])
        common, toadd, toremove = helpers.listdiff(sfdict.keys(), current.keys())
        existing, nonexisting, ignore = helpers.listdiff(
            toadd, allfeeds.keys())

        newfeeds = [Feed.create_new_feed(sfdict[f], f) for f in nonexisting]
        feedlist.extend(self.feedlist, newfeeds, from_sub=True) # will call extend_feed
        self.extend_feed([allfeeds[f] for f in existing], True)

        for f in toremove:
            index = self.index_feed(allfeeds[f])
            member = self.feedlist[index]
            if member.from_subscription:
                self.remove_feed(allfeeds[f])
        return

    def _subscription_changed(self, *args):
        self.emit('changed')

    def _subscription_contents_updated(self, *args):
        self.read_contents_from_subscription()

    def __str__(self):
        return "FeedCategory %s" % self.title

    def __hash__(self):
        return hash(id(self))

    def append_feed(self, value, from_sub):
        self.feedlist.append(CategoryMember(value, from_sub))
        self.emit('feed-added', value)

    def extend_feed(self, values, from_sub):
        self.feedlist.extend([CategoryMember(v, from_sub) for v in values])
        self.emit('changed')

    def insert_feed(self, index, value, from_sub):
        self.feedlist.insert(index, CategoryMember(value, from_sub))
        self.emit('feed-added', value)

    def remove(self, value):
        self.feedlist.remove(value)
        self.emit('feed-removed', value.feed)

    def remove_feed(self, value):
        for index, member in enumerate(self.feedlist):
            if member.feed is value:
                del self.feedlist[index]
                break
        else:
            raise ValueError(value)
        self.emit('feed-removed', value)

    def reverse(self):
        self.feedlist.reverse()
        self.emit('changed') # reverse=True))

    def index_feed(self, value):
        for index, f in enumerate(self.feedlist):
            if self.feedlist[index].feed is value:
                return index
        raise ValueError(value)

    def _sort_dsu(self, seq):
        aux_list = [(x.feed.title.lower(), x) for x in seq]
        aux_list.sort(lambda a,b:locale.strcoll(a[0],b[0]))
        return [x[1] for x in aux_list]

    def sort(self, indices=None):
        if not indices or len(indices) == 1:
            self.feedlist[:] = self._sort_dsu(self.feedlist)
        else:
            items = self._sort_dsu(indices)
            for i,x in enumerate(items):
                list.__setitem__(self.feedlist, indices[i], items[i])
        self.emit('changed')

    def move_feed(self, source, target):
        if target > source:
            target -= 1
        if target == source:
            return
        t = self[source]
        del self[source]
        self.feedlist.insert(target, t)
        self.emit('changed')

    def dump(self):
        head = {'title': self.title}
        if self.subscription is not None:
            head['subscription'] = self.subscription.dump()
        return [head] + [
            {'id': f.feed.id, 'from_subscription': f.from_subscription}
            for f in self.feedlist]

    @property
    def feeds(self):
        return [f.feed for f in self.feedlist]

    def __eq__(self, ob):
        if isinstance(ob, types.NoneType):
            return 0
        elif isinstance(ob, FeedCategory):
            return self.title == ob.title and list.__eq__(self.feedlist, ob)
        else:
            raise NotImplementedError

    def __contains__(self, item):
        error.log("warning, should probably be querying the feeds property instead?")
        return list.__contains__(self.feedlist, item)

class PseudoCategory(FeedCategory):
    def __init__(self, title="", key=None):
        if key not in (PSEUDO_ALL_KEY, PSEUDO_UNCATEGORIZED_KEY):
            raise ValueError, "Invalid key"
        FeedCategory.__init__(self, title)
        self._pseudo_key = key

    def __str__(self):
        return "PseudoCategory %s" % self.title

    def dump(self):
        return [{'pseudo': self._pseudo_key, 'title': ''}] + [
            {'id': f.feed.id, 'from_subscription': False} for f in self.feedlist]

    def append_feed(self, feed, from_sub):
        assert not from_sub
        FeedCategory.append_feed(self, feed, False)

    def insert_feed(self, index, feed, from_sub):
        assert not from_sub
        FeedCategory.insert_feed(self, index, feed, False)

fclist = None

def get_category_list():
    global fclist
    if fclist is None:
        fclist = FeedCategoryList()
    return fclist

category_list = get_category_list()
