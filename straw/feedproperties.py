""" FeedPropertiesDialog.py

Provides a module for handling the properties of a feed
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """
Straw is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

Straw is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place - Suite 330, Boston, MA 02111-1307, USA. """


import os, os.path
import gettext
from error import log, logparam, logtb
from xml.sax import saxutils
import gtk
from gtk.glade import XML
import gobject
import time

import Config
import SummaryParser
import MVP

import straw
from straw import helpers
from straw import feeds

class FeedPropertyView(MVP.GladeView):

    COLUMN_NAME = 0
    COLUMN_MEMBER = 1
    COLUMN_OBJECT = 2

    def _initialize(self):
        self._feed = None
        self._defaults = {'title':'','url':'','username':'','password':''}
        self._window = self._widget.get_widget('feed_properties')
        self._title = self._widget.get_widget('properties_title_entry')
        self._location = self._widget.get_widget('properties_location_entry')
        self._username = self._widget.get_widget('properties_username_entry')
        self._password = self._widget.get_widget('properties_password_entry')

        self._next_refresh_label = self._widget.get_widget('properties_next_refresh_label')
        self._previous_refresh_label = self._widget.get_widget('properties_previous_refresh_label')

        self._restore_button = self._widget.get_widget('properties_reset_button')
        self._refresh_spin = self._widget.get_widget('properties_refresh_spin')
        self._articles_spin = self._widget.get_widget('properties_articles_spin')
        self._refresh_default_check = self._widget.get_widget('properties_keep_refresh_default')
        self._articles_default_check = self._widget.get_widget('properties_keep_articles_default')

        self._categories_treeview = self._widget.get_widget('feed_categories_treeview')

        self._feed_info_description = self._widget.get_widget('feed_info_description')
        self._feed_info_copyright = self._widget.get_widget('feed_info_copyright')
        self._feed_info_link_box = self._widget.get_widget('feed_info_link_box')

        model = gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_BOOLEAN,
                              gobject.TYPE_PYOBJECT)
        self._categories_treeview.set_model(model)
        self._categories_treeview.set_rules_hint(True)

        r = gtk.CellRendererToggle()
        r.connect('toggled', self.member_toggled)
        column = gtk.TreeViewColumn(_('Member'), r, active=self.COLUMN_MEMBER)
        self._categories_treeview.append_column(column)

        r = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('Name'), r, text=self.COLUMN_NAME)
        self._categories_treeview.append_column(column)

    def set_feed(self, feed):
        self._feed = feed

    def member_toggled(self, cell, path):
        model = self._categories_treeview.get_model()
        category = model[path][self.COLUMN_OBJECT]
        if self._feed in category.feeds:
            category.remove_feed(self._feed)
            model[path][self.COLUMN_MEMBER] = False
        else:
            category.append_feed(self._feed, False)
            model[path][self.COLUMN_MEMBER] = True

    def show(self):
        self._initializing_window = True
        self.display_properties()
        self._window.present()
        self._initializing_window = False

    def hide(self, *args):
        self._presenter.activate_deleter()
        self._window.hide()
        self._window.destroy()

    def _on_feed_properties_delete_event(self, *args):
        self.hide()
        return True

    def _on_properties_close_button_clicked(self, *args):
        self.hide()
        return

    def display_properties(self):
        self._defaults['title'] = self._feed.title
        loc, un, pw = self._feed.access_info
        self._defaults['url'] = loc
        self._defaults['username'] = un
        self._defaults['password'] = pw
        self._defaults['frequency'] = self._feed.poll_frequency
        self._defaults['stored'] = self._feed.number_of_items_stored

        self._window.set_title(_("%s Properties") % self._feed.title)
        self._title.set_text(self._feed.title)
        self._location.set_text(self._feed.access_info[0])

        config = Config.get_instance()
        if self._feed.poll_frequency == feeds.Feed.DEFAULT:
            freq = config.poll_frequency
            fdefault = True
            self._refresh_keep = True
        else:
            freq = self._feed.poll_frequency
            fdefault = False
            self._refresh_keep = False
        self._refresh_spin.set_value(float(freq / 60))
        self._refresh_spin.set_sensitive(not fdefault)
        self._refresh_default_check.set_active(fdefault)
        if self._feed.number_of_items_stored == feeds.Feed.DEFAULT:
            nitems = config.number_of_items_stored
            nidefault = True
            self._articles_keep = True
        else:
            nitems = self._feed.number_of_items_stored
            nidefault = False
            self._articles_keep = False
        self._articles_spin.set_value(float(nitems))
        self._articles_spin.set_sensitive(not nidefault)
        self._articles_default_check.set_active(nidefault)

        if un:
            self._username.set_text(un)
        if pw:
            self._password.set_text(pw)

        model = self._categories_treeview.get_model()
        model.clear()
        fclist = feeds.category_list
        for c in fclist.user_categories:
            iter = model.append()
            model.set(iter,
                      self.COLUMN_NAME, c.title,
                      self.COLUMN_MEMBER, self._feed in c.feeds,
                      self.COLUMN_OBJECT, c)

        self._previous_refresh_label.set_text(
            helpers.format_date(time.gmtime(self._feed.last_poll)))
        next = self._feed.next_refresh
        if next:
            self._next_refresh_label.set_text(
                helpers.format_date(time.gmtime(next)))
            self._next_refresh_label.show()
        else:
            self._next_refresh_label.hide()

        self._display_feed_information(self._feed)
        self._restore_button.set_sensitive(False)

    def _display_feed_information(self, feed):
        title = helpers.convert_entities(feed.channel_title)
        if len(title) == 0:
            title = feed.title or feed.channel_link
        title = title.strip()

        link = feed.channel_link.strip()
        if not link: link = feed.location

        if link:
            link_button = gtk.LinkButton(link, link)
            self._feed_info_link_box.pack_end(link_button)
            gtk.link_button_set_uri_hook(self._load_uri, link)
        self._feed_info_link_box.show_all()

        description = helpers.convert_entities(feed.channel_description.strip())
        if description and description != title:
            description = read_text(description, len(description))
            self._feed_info_description.set_text(helpers.convert_entities(description))
            self._feed_info_description.show()
        else:
            self._feed_info_description.hide()

        copyright = helpers.convert_entities(feed.channel_copyright)
        if copyright:
            self._feed_info_copyright.set_text(copyright)
            self._feed_info_copyright.show()
        else:
            self._feed_info_copyright.hide()

    def _load_uri(self, widget, event, data):
        helpers.url_show(data)

    def restore_defaults(self):
        # FIXME: add frequency and number of items and the default flags
        self._feed.title = self._defaults['title']
        self._feed.access_info = (
            self._defaults['url'], self._defaults['username'], self._defaults['password'])
        self._title.set_text(self._feed.title)
        self._location.set_text(self._feed.location)
        self._username.set_text(self._defaults.get('username',''))
        self._password.set_text(self._defaults.get('password',''))
        self._refresh_default_check.set_active(self._refresh_keep)
        if not self._refresh_keep:
            self._refresh_spin.set_value(float(self._refresh_default))
        self._articles_default_check.set_active(self._articles_keep)
        if not self._articles_keep:
            self._articles_spin.set_value(float(self._articles_default))
        self._restore_button.set_sensitive(False)

    def _on_properties_title_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_title_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_title_entry_focus_out_event(self, widget, *args):
        self._feed.title = widget.get_text().strip()

    def _on_properties_location_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_location_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_location_entry_focus_out_event(self, widget, *args):
        loc, username, pw = self._feed.access_info
        self._feed.access_info = (widget.get_text().strip(), username, pw)

    def _on_properties_username_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_username_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(False)

    def _on_properties_username_entry_focus_out_event(self, widget, *args):
        self._presenter.set_username(widget.get_text().strip())

    def _on_properties_password_entry_insert_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_password_entry_delete_text(self, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(False)

    def _on_properties_password_entry_focus_out_event(self, widget, *args):
        self._presenter.set_password(widget.get_text().strip())

    def _on_properties_reset_button_clicked(self, *args):
        self.restore_defaults()

    def _on_properties_refresh_spin_focus_out_event(self, widget, *args):
        widget.update()
        value = widget.get_value_as_int()
        self._presenter.set_poll_frequency(value)

    def _on_properties_articles_spin_focus_out_event(self, widget, *args):
        widget.update()
        value = widget.get_value_as_int()
        self._presenter.set_items_stored(value)

    def _on_properties_articles_spin_value_changed(self, widget, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_refresh_spin_value_changed(self, widget, *args):
        if self._initializing_window:
            return
        self._restore_button.set_sensitive(True)

    def _on_properties_keep_refresh_default_toggled(self, widget, *args):
        if self._initializing_window:
            return
        isactive = widget.get_active()
        if isactive:
            self._presenter.set_poll_frequency(feeds.Feed.DEFAULT)
            self._refresh_spin.set_value(float(Config.get_instance().poll_frequency / 60))
        else:
            self._presenter.set_poll_frequency(self._refresh_spin.get_value_as_int() * 60)

        self._refresh_spin.set_sensitive(not isactive)
        self._restore_button.set_sensitive(True)

    def _on_properties_keep_articles_default_toggled(self, widget, *args):
        if self._initializing_window:
            return
        isactive = widget.get_active()
        if isactive:
            self._presenter.set_items_stored(feeds.Feed.DEFAULT)
            self._articles_spin.set_value(float(Config.get_instance().number_of_items_stored))
        else:
            self._presenter.set_items_stored(self._articles_spin.get_value_as_int())
        self._articles_spin.set_sensitive(not isactive)
        self._restore_button.set_sensitive(True)


class FeedPropertyPresenter(MVP.BasicPresenter):

    TIME_INTERVAL = 60

    def _initialize(self):
        self._feed = None
        self._deleter = None

    def set_deleter(self, deleter):
        self._deleter = deleter

    def activate_deleter(self):
        if self._deleter:
            self._deleter()

    def set_feed(self, feed):
        self._feed = feed
        self._view.set_feed(self._feed)

    def set_username(self, username):
        loc, uname, pw = self._feed.access_info
        self._feed.access_info = (loc, username, pw)

    def set_password(self, password):
        loc, uname, pw = self._feed.access_info
        self._feed.access_info = (loc, uname, password)

    def set_poll_frequency(self, pf):
        if pf != self._feed.poll_frequency:
            self._feed.poll_frequency = pf * FeedPropertyPresenter.TIME_INTERVAL

    def set_items_stored(self, nitems):
        if nitems != self._feed.number_of_items_stored:
            self._feed.number_of_items_stored = nitems

dialogs = {}
def feed_properties_show(feed):
    global dialogs
    class _dialogdeleter:
        def __init__(self, feed, hash):
            self.feed = feed
            self.hash = hash

        def __call__(self):
            del self.hash[feed]

    dialog = dialogs.get(feed, None)
    if not dialog:
        gladefile = XML(os.path.join(straw.STRAW_DATA_DIR, "feed-properties.glade"))
        #idget_tree = gladefile.get_widget_tree('feed_properties')
        dialog = FeedPropertyPresenter(view=FeedPropertyView(gladefile))
        dialog.set_feed(feed)
        dialog.set_deleter(_dialogdeleter(feed, dialogs))
        dialogs[feed] = dialog
    dialog.view.show()
    return

def read_text(fragment, chars):
    """Read chars cdata characters from html fragment fragment"""
    parser = SummaryParser.TitleImgParser()
    parser.feed(fragment)
    text = parser.get_text(chars)
    parser.close()
    return text
