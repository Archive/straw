""" ItemList.py

Handles listing of feed items in a view (i.e. GTK+ TreeView)
"""
__copyright__ = "Copyright (c) 2002-2005 Free Software Foundation, Inc."
__license__ = """GNU General Public License

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public
License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
"""

import os.path, operator
from xml.sax import saxutils
import pygtk
pygtk.require('2.0')
import gobject
import gtk
import pango
import MVP
import error
import Config

from straw import helpers

class Column:
    """
    Constants for the item list treeview columns
    """
    title, sticky, item, weight, sticky_flag, foreground = range(6)

class ItemListModel:
    ''' A model of summary items for tree views '''

    def __init__(self):
        self.store = gtk.ListStore(str, gobject.TYPE_OBJECT, gobject.TYPE_PYOBJECT, int, bool, str)
        config = Config.get_instance()
        self.item_order = config.item_order

    def populate(self, summaryItems):
        print "POPULATE ", len(summaryItems)
        ## XXX should we sort here or in feeds.Feed?
        items = sorted(summaryItems, key=operator.attrgetter('pub_date'),
                       reverse=self.item_order)
        self.store.clear()
        for item in items:
            weight = (pango.WEIGHT_BOLD, pango.WEIGHT_NORMAL)[item.seen]
            colour = ("#0000FF", "#000000")[item.seen]
            treeiter = self.store.append()
            self.store.set(treeiter,
                            Column.title, item.title,
                            Column.item, item,
                            Column.weight, weight,
                            Column.sticky_flag, item.sticky,
                            Column.foreground, colour)
            item.connect('changed', self.item_changed_cb)

    def item_changed_cb(self, item):
        for row in self.store:
            rowitem = row[Column.item]
            if rowitem is item:
                row[Column.weight] = item.seen and pango.WEIGHT_NORMAL or pango.WEIGHT_BOLD
                row[Column.foreground] = item.seen and 'black' or 'blue'
                row[Column.sticky_flag] = item.sticky
                row[Column.item] = item

    @property
    def model(self):
        return self.store


class ItemListView(MVP.WidgetView):

    def _initialize(self):
        self._widget.set_rules_hint(False)
        self._widget.connect("button_press_event",self._on_button_press_event)
        self._widget.connect("popup-menu", self._on_popup_menu)
        self._widget.connect("row-activated", self._on_row_activated)
        uifactory = helpers.UIFactory('ItemListActions')
        action = uifactory.get_action('/itemlist_popup/mark_as_unread')
        action.connect('activate', self.on_menu_mark_as_unread_activate)
        self.popup = uifactory.get_popup('/itemlist_popup')

        renderer = gtk.CellRendererToggle()
        column = gtk.TreeViewColumn(_('Keep'), renderer,
                                    active=Column.sticky_flag)
        column.set_resizable(True)
        column.set_reorderable(True)
        self._widget.append_column(column)
        renderer.connect('toggled', self._sticky_toggled)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_('_Title'), renderer,
                                    text=Column.title,
                                    foreground=Column.foreground,
                                    weight=Column.weight)
        column.set_resizable(True)
        column.set_reorderable(True)
        self._widget.append_column(column)

        selection = self._widget.get_selection()
        selection.set_mode(gtk.SELECTION_SINGLE)
        selection.connect('changed', lambda x: self.item_mark_as_read(True))

    def add_selection_changed_listener(self, listener):
        selection = self._widget.get_selection()
        selection.connect('changed',
                          listener.itemlist_selection_changed,
                          Column.item)

    def _model_set(self):
        self._widget.set_model(self._model.model)

    def _sticky_toggled(self, cell, path):
        model = self._widget.get_model()
        treeiter = model.get_iter((int(path),))
        item = model.get_value(treeiter, Column.item)
        item.sticky = not item.sticky
        model.set(treeiter, Column.sticky_flag, item.sticky)

    def _on_popup_menu(self, treeview, *args):
        self.popup.popup(None,None,None,0,0)

    def _on_button_press_event(self, treeview, event):
        val = 0
        if event.button == 3:
            x = int(event.x)
            y = int(event.y)
            time = gtk.get_current_event_time()
            path = treeview.get_path_at_pos(x, y)
            if path is None:
                return 1
            path, col, cellx, celly = path
            treeview.grab_focus()
            treeview.set_cursor( path, col, 0)
            self.popup.popup(None, None, None, event.button, time)
            val = 1
        return val

    def on_menu_mark_as_unread_activate(self, *args):
        self.item_mark_as_read(False)

    def item_mark_as_read(self, isRead):
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter: return
        item = model.get_value(treeiter, Column.item)
        item.seen = isRead
        weight = (pango.WEIGHT_BOLD, pango.WEIGHT_NORMAL)[item.seen]
        colour = ("#0000FF", "#000000")[item.seen]
        model.set(treeiter, Column.foreground, colour,
                  Column.weight, weight,
                  Column.item, item)
        return

    def _on_row_activated(self, treeview, path, column):
        ''' double-clicking an item opens that item in the web browser '''
        model = self._widget.get_model()
        try:
            treeiter = model.get_iter(path)
        except ValueError:
            return
        link = model.get_value(treeiter, Column.ITEM).link
        if link:
            helpers.url_show(link)
        return

    def select_first_item(self):
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        path = model.get_path(model.get_iter_first())
        selection.select_path(path)
        return True

    def select_previous_item(self):
        """
        Selects the item before the current selection. If there
        is no current selection, selects the last item. If there is no
        previous row, returns False.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter: return False
        path = model.get_path(treeiter)
        prev = path[-1]-1
        if prev < 0:
            return False
        selection.select_path(prev)
        return True

    def select_next_item(self):
        """
        Selects the item after the current selection. If there is no current
        selection, selects the first item. If there is no next row, returns False.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        if not treeiter:
            treeiter = model.get_iter_first()
        next_iter = model.iter_next(treeiter)
        if not next_iter or not model.iter_is_valid(treeiter):
            return False
        next_path = model.get_path(next_iter)
        selection.select_path(next_path)
        return True

    def select_last_item(self):
        """
        Selects the last item in this list.
        """
        selection = self._widget.get_selection()
        selection.select_path(len(self.model.model) - 1)
        return True

    def select_next_unread_item(self):
        """
        Selects the first unread item after the current selection. If there is
        no current selection, or if there are no unread items, returns False..

        By returning False, the caller can either go to the next feed, or go
        to the next feed with an unread item.
        """
        selection = self._widget.get_selection()
        (model, treeiter) = selection.get_selected()
        # check if we have a selection. if none,
        # start searching from the first item
        if not treeiter:
            treeiter = model.get_iter_first()
        if not treeiter: return False
        nextiter = model.iter_next(treeiter)
        if not nextiter: return False # no more rows to iterate
        treerow = model[nextiter]
        has_unread = False
        while(treerow):
            item = treerow[Column.item]
            if not item.seen:
                has_unread = True
                selection.select_path(treerow.path)
                break
            treerow = treerow.next
        return has_unread

class ItemListPresenter(MVP.BasicPresenter):

    ## XXX listen for RefreshFeedDisplaySignal, FeedOrder changes,

    def _initialize(self):
        self.model = ItemListModel()

    def feedlist_selection_changed(self, feedlist_selection, column):
        (model, pathlist) = feedlist_selection.get_selected_rows()
        iters = [model.get_iter(path) for path in pathlist]
        if not iters:
            return
        nodes = [model.get_value(treeiter, column) for treeiter in iters]
        items = []
        try:
            items += [node.feed.items for node in nodes]
        except TypeError, te:
            gen = (node.category.feeds for node in nodes)
            items += [feed.items for feed in gen.next()]
        items = list(self.flatten(items))
        self.model.populate(items)
        if not len(self.model.model): return
        if not self.view.select_next_unread_item():
            self.view.select_first_item()

    def flatten(self, sequence):
        ''' python cookbook recipe 4.6 '''
        for item in sequence:
            if isinstance(item, (list, tuple)):
                for subitem in self.flatten(item):
                    yield subitem
            else:
                yield item

    def select_previous_item(self):
        return self.view.select_previous_item()

    def select_next_item(self):
        return self.view.select_next_item()

    def select_next_unread_item(self):
        return self.view.select_next_unread_item()

    def select_last_item(self):
        return self.view.select_last_item()

"""
    def display_feed_items(self, feed, select_first=1):
        redisplay = self._selected_feed is feed
        #if self._selected_feed and not redisplay:
        #    self._selected_feed.signal_disconnect(Event.RefreshFeedDisplaySignal,
        #                                 self._feed_order_changed)
        #    self._selected_feed.signal_disconnect(Event.AllItemsReadSignal,
        #                                 self._all_items_read)
        #    self._selected_feed.signal_disconnect(Event.ItemReadSignal,
        #                                 self._item_read)
        self._selected_feed = feed
        if not redisplay:
            #self._selected_feed.connect(Event.RefreshFeedDisplaySignal,
            #                    self._feed_order_changed)
            self._selected_feed.connect('items_read', self._all_items_read)
            #self._selected_feed.signal_connect(Event.ItemReadSignal,
            #                    self._item_read)
        count = self._render_feed(self._selected_feed)
        if not redisplay and count:
            if not self.select_next_unread_item():
                selection = self._view.get_widget_selection()
                selection.select_path((0,))
        return

    def _feed_order_changed(self, event):
        if event.sender is self._selected_feed:
            item = None
            selection = self._view.get_widget_selection()
            model, treeiter = selection.get_selected()
            if treeiter is not None:
                path = model.get_path(treeiter)
                item = model[path[0]][Column.ITEM]
            self._render_feed(event.sender)
            if item:
                path = (item.feed.get_item_index(item),)
            else:
                path = (0,) # first item
            selection.select_path(path)

    def _all_items_read(self, feed, unread_items):
        print unread_items
        for index, item in enumerate(unread_items):
            self._mark_item(item, index)

    def _item_read(self, signal):
        self._mark_item(signal.item)

    def _render_feed(self, feed):
        self._model.clear()
        curr_iter = None
        for item in feed.items:
            weight = (pango.WEIGHT_BOLD, pango.WEIGHT_NORMAL)[item.seen]
            colour = ("#0000FF", "#000000")[item.seen]
            treeiter = self._model.append()
            self._model.set(treeiter,
                            Column.title, item.title,
                            Column.ITEM, item,
                            Column.WEIGHT, weight,
                            Column.sticky_flag, item.sticky,
                            Column.FOREGROUND, colour)
            if item is self._selected_item:
                curr_iter = treeiter

        if curr_iter:
            path = self._model.get_path(curr_iter)
            self._view.scroll_to_cell(path)
        return len(feed.items)

    def _mark_item(self, item, path=None):
        if path is None:
            path = (item.feed.get_item_index(item),)
        self._set_column_weight(item, path)
        return
"""
